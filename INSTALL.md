# Pronto API Installation

## Requirements

* Web Server: Developed and tested under Apache, but should work with nginx
* PHP: Version 5.4+
* PDO_INFORMIX: Enable the extension in PHP (beyond the scope of this document)
* Read-only ODBC Credentials to Informix (beyond the scope of this document)

## Steps

* `git clone git@gitlab.com:natures-organics/proapi.git`
* `cd proapi`
* `composer install`
* `cp proapi.cfg proapi.cfg.local`
* `vim proapi.cfg.local`
  * Configure required directives.
  * Remove unchanged directives.
* Configure Virtual Host in Web Server.

## API Keys

To create API Keys, edit `proapi.cfg.local` and create an section titled
`[apikeys]`, then add one line per key formatted as:

    [key].name = description
    [key].db   = permitted Pronto/Informix database

For example, to create API Key 1234567891234567 for use by Acme Enterprises
with access to the L01 database:

    1234567891234567.name = Acme Enterprises
    1234567891234567.db   = L01

**Important:** API Keys must be a minimum of 16 characters, and can consist of
any valid ASCII characters, although we recommend using only standard
alpha-numeric characters (ie, A-z0-9) to avoid complications for client
implementations.
