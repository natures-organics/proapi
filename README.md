# Pronto API Documentation

This is an internally developed API for Pronto. It provides a read-only
interface to programatically extract data from our ERP system. Data for most
modules are exposed to some degree, however Financial and General Ledger
data is **not** available via this API. This is by-design to minimize the risk
of accidental leakage of confidential data.

## Authentication

All requests must be authenticated using an API key within the request. This can
be supplied either in the `X-Apikey` Header, or in the `apikey` GET parameter.
**It is preferred to use the Header method in production systems.**

### Header example using curl

* `curl -H "X-Apikey: abc123" 'https://prontoapi.example.com/v1/suppliers?group=svs'`

### GET parameter examples:

* `https://prontoapi.example.com/v1/sys/sq?apikey=abc123`
* `https://prontoapi.example.com/v1/customers?apikey=abc123&territory=WW`

## Responses

### Data Payload

All responses with data to return are formatted in a common JSON structure:

* `COUNT`: Number of records returned in the `results` array.
* `SERVER`: The hostname of the server that generated the response.
* `DB`: Details of the database used to retrieve results.
* `TZ`: Timestamp the response was generated (in the servers timezone).
* `RESULTS`: Object (or array of objects) of response data.

Example JSON data with 8 rows of data returned in `RESULTS` (*omitted from this
example*), generated on server `apiserver.example.com` using databse `l01` on
server `pronto.example.com` at time 16:07 on 19 Sept 2019 GMT+10.

    "COUNT": 8,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:07:54+10:00",
    "RESULTS":
      {...}

### Codes

The following HTTP responses are returned by the API as appropriate:

* `200`: Request was successful, with results.
* `204`: Request was successful, but resulted in no data to return.
* `400`: Invalid or missing parameters. Some endpoints require at least one
filtering parameter to avoid returning entire (large) database tables.
* `401`: Authentication Key was not supplied.
* `403`: Authentication Key was supplied, but was not valid.
* `406`: Unknown format requested for response (possibly caused by incorrect
"Accept" HTTP header in your request).
* `413`: Number of results exceeded the maximum number of results the API will
return.
* `422`: Supplied parameter was not able to be parsed as valid data. Examples
include Sales Order Numbers that don't match the Pronto SO Number pattern
"NNNNNAA" where N = Number, A = Alpha.
* `501`: Endpoint is valid, but certain specifics of the request have not been
implemented within the API (yet).

## Versioning

API is versioned, with the current version being `v1`. All endpoints are
prefixed with this version.

Future versions will be prefixed with incremental version numbers to avoid
breakage of existing clients as the API is improved.

---

# Endpoint Summary

Quick reference of all available endpoints. Refer to full documentation of each
endpoint for a complete explanation of usage and responses.

## Customers

* [Customer Index](#endpoint-customers-index) : `GET /v1/customers`
* [Customer Header](#endpoint-customers-header) : `GET /v1/customers/:pk`
* [Customer Shortships](#endpoint-customers-shortships) : `GET /v1/customers/:pk/shortships`

## Suppliers

* [Supplier Index](#endpoint-suppliers-index) : `GET /v1/suppliers`
* [Supplier Header](#endpoint-suppliers-header) : `GET /v1/suppliers/:pk`

## Products

* [Product Index](#endpoint-products-index) : `GET /v1/products/`
* [Product Header](#endpoint-products-header) : `GET /v1/products/:pk`
* [Product Units of Measure](#endpoint-products-uoms) : `GET /v1/products/:pk/uom`
* [Product Unit of Measure](#endpoint-products-uom) : `GET /v1/products/:pk/uom/:key`
* [Product Warehouses](#endpoint-products-warehouses) : `GET /v1/products/:pk/warehouse`
* [Product Warehouse](#endpoint-products-warehouse) : `GET /v1/products/:pk/warehouse/:key`
* [Product Sales Orders](#endpoint-products-salesorders) : `GET /v1/products/:pk/salesorders`
* [Product Short Ships](#endpoint-products-shortships) : `GET /v1/products/:pk/shortships`

## Sales Orders

* [Sales Order Index](#endpoint-sorders-index) : `GET /v1/salesorders`
* [Sales Order Header](#endpoint-sorders-header) : `GET /v1/salesorders/:pk`
* [Sales Order Lines](#endpoint-sorders-lines) : `GET /v1/salesorders/:pk/lines`
* [Sales Order Packages](#endpoint-sorders-packages) : `GET /v1/salesorders/:pk/packages`
* [Package Detail](#endpoint-sorders-packages-header) : `GET /v1/salesorders/packages/:pk`

## Purchase Orders

* [Purchase Order Index](#endpoint-porders-index) : `GET /v1/purchaseorders`
* [Purchase Order Header](#endpoint-porders-header) : `GET /v1/purchaseorders/:pk`
* [Purchase Order Lines](#endpoint-porders-lines) : `GET /v1/purchaseorders/:pk/lines`

## Bill of Materials

* [BoM Index](#endpoint-bom-index) : `GET /v1/boms`
* [BoM Header](#endpoint-bom-header) : `GET /v1/boms/:pk`
* [BoM Lines](#endpoint-bom-lines) : `GET /v1/boms/:pk/lines`

## Work Orders

* [Work Order Index](#endpoint-worders-index) : `GET /v1/workorders`
* [Work Order Header](#endpoint-worders-header) : `GET /v1/workorders/:pk`
* [Work Order Lines](#endpoint-worders-lines) : `GET /v1/workorders/:pk/lines`
* [Work Order Dockets](#endpoint-worders-dockets) : `GET /v1/workorders/:pk/dockets`

## Code Tables

* [System Code Tables](#endpoint-system-code-tables) : `GET /v1/sys/:pk`

---

# Endpoint: Products

<a name="endpoint-products-index"></a>
## Index

Returns an index of basic data about matching product master data. Can be
filtered using parameters to identify a subset of products. Use the returned
`ID` field to retrieve detailed data using the `Detail` endpoint.

* URL: `/v1/products`
* Method: `GET`

### Parameters

The following GET parameters are supported:

* Optional: `description`
    * String
    * Keyword matche partial string
* Optional: `brand`
    * String[2]
    * Brand ID - System 'B6' Table
* Optional: `group`
    * String[2]
    * Group ID - System 'PG' Table
* Optional: `status`
    * String[1]
    * Status Code
* Optional: `condition`
    * String[1]
    * Condition Code
* Optional: `type`
    * String[2]
    * Holding Type Code

### Response

Example response to `GET /v1/products?description=Lemongrass&type=fg`

```json
{
    "COUNT": 3,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "ID": "FI855344",
            "DESCRIPTION": "G/A Lemongrass Dishwash 500ml",
            "CONDITION": "I",
            "BRAND_ID": "85",
            "GROUP_ID": "53",
            "STATUS_ID": "S",
            "TYPE_ID": "FG"
        },
        {
            "ID": "FM123456",
            "DESCRIPTION": "OC H/Wash Lemongrass 250ml",
            "CONDITION": " ",
            "BRAND_ID": "11",
            "GROUP_ID": "22",
            "STATUS_ID": "S",
            "TYPE_ID": "FG"
        },
        {
            "ID": "FM654321",
            "DESCRIPTION": "EC Dish Conc Lemongrass 500ml",
            "CONDITION": " ",
            "BRAND_ID": "14",
            "GROUP_ID": "51",
            "STATUS_ID": "S",
            "TYPE_ID": "FG"
        }
    }
}

```

<a name="endpoint-products-header"></a>
## Header

Returns detailed product master data.

* URL: `/v1/product/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product to retrieve full data for.

### Response

Example response to `GET /v1/product/FM123456`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "FM123456": {
            "ALPHACODE": "H2A",
            "HOLDING_TYPE": "FG",
            "SHELF_LIFE": "30 Months",
            "NOMINAL_VOLUME": 250,
            "DESCRIPTION": {
                "LINE1": "OC H/Wash 250ml",
                "LINE2": null,
                "LINE3": null
            },
            "STATUS": {
                "ID": "S",
                "DESCRIPTION": "Stocked"
            },
            "CONDITION": {
                "ID": null,
                "DESCRIPTION": "Active"
            },
            "SERIALIZED": {
                "ID": "N",
                "DESCRIPTION": "Not Tracked"
            },
            "GROUP": {
                "ID": "22",
                "DESCRIPTION": "Soaps: Organics Core"
            },
            "BRAND": {
                "ID": "11",
                "DESCRIPTION": "Organics Core"
            },
            "UOM": {
                "BASE": {
                    "ID": "CT24",
                    "DESCRIPTION": "Carton of 24 Units",
                    "TUN": "19123421341234"
                },
                "PACK": {
                    "ID": "PLT",
                    "DESCRIPTION": "Pallet",
                    "QUANTITY": 72,
                    "WEIGHT": 512.2,
                    "LENGTH": 1.165,
                    "WIDTH": 1.165,
                    "HEIGHT": 1.3,
                    "CUBIC": 1.7643925,
                    "TUN": "74562544352121"
                }
            },
            "REORDER_BUYER": {
                "ID": "F1",
                "DESCRIPTION": "Filler 1"
            },
            "DATES": {
                "CREATED": "2016-01-01",
                "UPDATED": "2019-05-07"
            }
        }
    }
}
```

<a name="endpoint-products-lots"></a>
## Lots (Batch Tracking)

Returns known lot numbers of product.

* URL: `/v1/product/:pk/lots
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product.

### Response

TODO
Example response to `GET /v1/product/FM110207/uom`:

```json
{
    "COUNT": 2,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "CT24": {
            "CONVERSION_FACTOR": 1,
            "TRADE_UNIT_NO": "19310692476129",
            "TUN_STATUS": null,
            "OWN_BOX_CODE": null,
            "PUBLISH_FLAG": null,
            "PRIMARY_UOM": true,
            "PACK_UOM": false,
            "DIMENSIONS": {
                "LENGTH": 0.534,
                "WIDTH": 0.196,
                "HEIGHT": 0.19,
                "CUBIC": 0.01988616,
                "WEIGHT": 6.6
            }
        },
        "CU": {
            "CONVERSION_FACTOR": 0.0416667,
            "TRADE_UNIT_NO": "9310692476122",
            "TUN_STATUS": null,
            "OWN_BOX_CODE": null,
            "PUBLISH_FLAG": null,
            "PRIMARY_UOM": false,
            "PACK_UOM": false,
            "DIMENSIONS": {
                "LENGTH": 0,
                "WIDTH": 0,
                "HEIGHT": 0,
                "CUBIC": 0,
                "WEIGHT": 0.275
            }
        }
    }
}
```

<a name="endpoint-products-uoms"></a>
## Units of Measure (UOMs)

Returns alternate Units of Measure and associated details for given product.

* URL: `/v1/product/:pk/uom`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product.

### Response

Example response to `GET /v1/product/FM123456/uom`:

```json
{
    "COUNT": 2,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "CT24": {
            "CONVERSION_FACTOR": 1,
            "TRADE_UNIT_NO": "35847684353129",
            "TUN_STATUS": null,
            "OWN_BOX_CODE": null,
            "PUBLISH_FLAG": null,
            "PRIMARY_UOM": true,
            "PACK_UOM": false,
            "DIMENSIONS": {
                "LENGTH": 0.534,
                "WIDTH": 0.196,
                "HEIGHT": 0.19,
                "CUBIC": 0.01988616,
                "WEIGHT": 6.6
            }
        },
        "CU": {
            "CONVERSION_FACTOR": 0.0416667,
            "TRADE_UNIT_NO": "8643543576122",
            "TUN_STATUS": null,
            "OWN_BOX_CODE": null,
            "PUBLISH_FLAG": null,
            "PRIMARY_UOM": false,
            "PACK_UOM": false,
            "DIMENSIONS": {
                "LENGTH": 0,
                "WIDTH": 0,
                "HEIGHT": 0,
                "CUBIC": 0,
                "WEIGHT": 0.275
            }
        }
    }
}
```

<a name="endpoint-products-uom"></a>
## Units of Measure (UOMs) - Specific

Returns specific alternate Unit of Measure and associated details for given product.

* URL: `/v1/product/:pk/uom/:key`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product.
* Required: `:key` the ID of the unit of measure.

### Response

Example response to `GET /v1/product/FM123456/uom/cu`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "CU": {
            "CONVERSION_FACTOR": 0.0416667,
            "TRADE_UNIT_NO": "8724256842345",
            "TUN_STATUS": null,
            "OWN_BOX_CODE": null,
            "PUBLISH_FLAG": null,
            "PRIMARY_UOM": false,
            "PACK_UOM": false,
            "DIMENSIONS": {
                "LENGTH": 0,
                "WIDTH": 0,
                "HEIGHT": 0,
                "CUBIC": 0,
                "WEIGHT": 0.275
            }
        }
    }
}
```

<a name="endpoint-products-warehouses"></a>
## Warehouses

Returns warehouse holding details for given product.

* URL: `/v1/product/:pk/warehouse`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product.

### Response

Example response to `GET /v1/product/FM123456/warehouse`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "FTGF": {
            "UOM": "CT24",
            "BIN_LOCATION": null,
            "QUANTITY": {
                "ON_HAND": 615,
                "PICKED": 0,
                "ON_HAND_LIVE": 615,
                "SALES_ORDERS": 0,
                "BACK_ORDERS": 0,
                "FORWARD_ORDERS": 0,
                "AVAILABLE": 615,
                "PURCHASE_ORDERS": 792
            },
            "DEMAND": {
                "THIS_MONTH": 103,
                "MONTHLY_AVERAGE": 524.25,
                "DAYS_COVER": 35.2,
                "ACTIVE_MONTHS": 4,
                "ACTIVE_PERIOD": "ROLLING"
            },
            "DATES": {
                "LAST_SALE": "2019-09-17",
                "LAST_STOCKTAKE": "2019-06-28",
                "LAST_CHANGE": "2019-09-17"
            }
        }
    }
}
```

<a name="endpoint-products-warehouse"></a>
## Warehouse Holdings

Returns specific warehouse holdings for given product.

* URL: `/v1/product/:pk/warehouse/:key`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product.
* Required: `:key` the ID of the warehouse.

### Response

Example response to `GET /v1/product/FM123456/warehouse/ftgf`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "FTGF": {
            "UOM": "CT24",
            "BIN_LOCATION": null,
            "QUANTITY": {
                "ON_HAND": 615,
                "PICKED": 0,
                "ON_HAND_LIVE": 615,
                "SALES_ORDERS": 0,
                "BACK_ORDERS": 0,
                "FORWARD_ORDERS": 0,
                "AVAILABLE": 615,
                "PURCHASE_ORDERS": 792
            },
            "DEMAND": {
                "THIS_MONTH": 103,
                "MONTHLY_AVERAGE": 524.25,
                "DAYS_COVER": 35.2,
                "ACTIVE_MONTHS": 4,
                "ACTIVE_PERIOD": "ROLLING"
            },
            "DATES": {
                "LAST_SALE": "2019-09-17",
                "LAST_STOCKTAKE": "2019-06-28",
                "LAST_CHANGE": "2019-09-17"
            }
        }
    }
}
```

<a name="endpoint-products-salesorders"></a>
## Sales Orders

Returns sales orders for given product.

* URL: `/v1/product/:pk/salesorders`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product.
* Optional: `status`
  * Integer
  * Order Status (eg, '30' for 'Ready to Print' orders)
* Optional: `customer`
  * String
  * Customer ID.

### Response

Example response to `GET /v1/product/fm123456/salesorders?status=40`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "LINE_SEQ": 3,
            "STATUS": "40",
            "CUSTID": "JBVIC",
            "ORDER_NUM": "63222",
            "QUANTITY": {
                "ORDERED": 288,
                "SHIPPED": 288
            }
        },
        {
            "LINE_SEQ": 2,
            "STATUS": "40",
            "CUSTID": "JBVIC",
            "ORDER_NUM": "63223",
            "QUANTITY": {
                "ORDERED": 96,
                "SHIPPED": 96
            }
        },
        {
            "LINE_SEQ": 2,
            "STATUS": "40",
            "CUSTID": "JBQLD",
            "ORDER_NUM": "63224",
            "QUANTITY": {
                "ORDERED": 480,
                "SHIPPED": 480
            }
        }
    }
}
```

<a name="endpoint-products-shortships"></a>
## Short Ships

Returns short-ship events for given product.

* URL: `/v1/product/:pk/shortships`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the product.
* Optional: `customer`:
  * String
  * Customer ID
* Optional: `reason`:
  * String[1]
  * Short Ship Reason (see 'SQ' system code table)

### Response

Example response to `GET /v1/product/FM123456/shortships?reason=3`.

Object key of the returned JSON is the sales order number.

```json
{
    "COUNT": 3,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "10384": {
            "LINE_SEQ": 14,
            "PRODUCT_ID": "FM1234567",
            "CUSTOMER_ID": "WWQLD",
            "QUANTITY": {
                "ORDERED": 144,
                "SHIPPED": 116,
                "SHORTED": 28
            },
            "SHORT_SHIP": {
                "CODE": "3",
                "REASON": "Unable to Locate"
            }
        },
        "10913": {
            "LINE_SEQ": 8,
            "PRODUCT_ID": "FM123456",
            "CUSTOMER_ID": "MCNSW",
            "QUANTITY": {
                "ORDERED": 144,
                "SHIPPED": 83,
                "SHORTED": 61
            },
            "SHORT_SHIP": {
                "CODE": "3",
                "REASON": "Unable to Locate"
            }
        },
        "10966": {
            "LINE_SEQ": 6,
            "PRODUCT_ID": "FX123456",
            "CUSTOMER_ID": "MCWA",
            "QUANTITY": {
                "ORDERED": 52,
                "SHIPPED": 0,
                "SHORTED": 52
            },
            "SHORT_SHIP": {
                "CODE": "3",
                "REASON": "Unable to Locate"
            }
        }
    }
}
```

---

# Endpoint: Bill of Materials

<a name="endpoint-bom-index"></a>
## Index

Returns an index of Bills of Materials (BOM's). Can be filtered using parameters
to identify a subset of BOM's. Use the returned `ID` field to retrieve detailed
data using the `Detail` endpoint.

* URL: `/v1/boms`
* Method: `GET`

### Parameters

The following GET parameters are supported:

* Optional: `product`
    * String
    * Product ID
* Optional: `type`
    * String[1]
    * BOM Type - Primary ('P') or Alternate ('N')

### Response

Example response to `GET /v1/boms?product=FM123456`

```json
{
    "COUNT": 2,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "ID": "75",
            "DESCRIPTION": "EC Dishwash Liq 1Ltr",
            "TYPE": "P",
            "STOCK_ID": "FM123456"
        },
        {
            "ID": "677",
            "DESCRIPTION": "EC Dishwash Liq 1Ltr (R3 Hybrid)",
            "TYPE": "N",
            "STOCK_ID": "FM123456"
        }
    }
}

```

<a name="endpoint-bom-header"></a>
## Header

Returns detailed BOM data.

* URL: `/v1/boms/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the BOM to retrieve full data for.

### Response

Example response to `GET /v1/boms/75`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "75": {
            "BOM_DESCRIPTION": "EC Dishwash Liq 1Ltr",
            "REVISION": null,
            "REFERENCE": null,
            "BOM_COMMENT": null,
            "FORMULATION_QTY": 96,
            "AUTHORIZED_FLAG": true,
            "YIELD_PCENT": 1,
            "PRODUCT": {
                "ID": "FM123456",
                "DESCRIPTION": "EC Dishwash Liq 1Ltr"
            },
            "TYPE": {
                "ID": "P",
                "DESCRIPTION": "PRIMARY"
            },
            "DATES": {
                "START": null,
                "END": "2079-06-06",
                "LAST_CHANGE": "2019-07-23"
            }
        }
    }
}
```

**Note:** `yield_pcent` is a decimal between 0 and 1.

<a name="endpoint-bom-lines"></a>
## Lines

Returns BOM lines for given ID.

* URL: `/v1/boms/:pk/lines`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the BOM to retrieve line data for.

### Response

Example response to `GET /v1/boms/75/lines`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "4": {
            "LINE_TYPE": null,
            "PRODUCT": {
                "ID": "LA141003B",
                "DESCRIPTION": "EC Dishwash 1ltr BCK"
            },
            "QUANTITY": {
                "BOM": 864,
                "UOM": "EACH"
            }
        },
        "8": {
            "LINE_TYPE": null,
            "PRODUCT": {
                "ID": "BT1002",
                "DESCRIPTION": "Clear 1L BTL"
            },
            "QUANTITY": {
                "BOM": 864,
                "UOM": "EACH"
            }
        },
        "24": {
            "LINE_TYPE": null,
            "PRODUCT": {
                "ID": "PM20",
                "DESCRIPTION": "Machine Roll"
            },
            "QUANTITY": {
                "BOM": 0.45,
                "UOM": "KG"
            }
        }
    }
}
```

---

# Endpoint: Customers

<a name="endpoint-customers-index"></a>
## Index

Returns an index of basic data about matching customer master data. Can be
filtered using parameters to identify a subset of customers. Use the returned
`ID` field to retrieve detailed data using the `Detail` endpoint.

* URL: `/v1/customers`
* Method: `GET`

### Parameters

The following GET parameters are supported:

* Optional: `territory`
    * String[2]
    * Territory Code (TC System Table)
* Optional: `type`
    * String[2]
    * Customer Type (CT System Table)
* Optional: `status`
    * String[1]
    * Status Code of Customer Account

### Response

Example response to `GET /v1/customers?territory=ww`

```json
{
    "COUNT": 3,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "ID": "WWVIC",
            "NAME": "Woolworths Victoria",
            "TERRITORY": "WW",
            "TYPE": "WW",
            "STATUS": null
        },
        {
            "ID": "WWNSW",
            "NAME": "Woolworths NSW",
            "TERRITORY": "WW",
            "TYPE": "WW",
            "STATUS": null
        },
        {
            "ID": "WWWA",
            "NAME": "Woolworths Western Australia",
            "TERRITORY": "WW",
            "TYPE": "WW",
            "STATUS": null
        }
    }
}

```

<a name="endpoint-customers-header"></a>
## Header

Returns detailed customer master data.

* URL: `/v1/customers/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the customer to retrieve full data for.

### Response

Example response to `GET /v1/customers/test99`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "TEST99": {
            "SHORTNAME": "Testing Customer 99",
            "STATUS": null,
            "CUSTNAME": "Testing Customer 99",
            "PARENT": {
                "ID": "TEST99",
                "SHORTNAME": "Testing Customer 99"
            },
            "ADDRESS": {
                "LINE1": "*** TESTING ***",
                "LINE2": "*** TESTING ***",
                "SUBURB": "*** TESTING ***",
                "POSTCODE": null,
                "COUNTRY": null
            },
            "DELIVERY": {
                "NAME": "Testing Customer 99",
                "LINE1": "** DO NOT PICK **",
                "LINE2": "** DO NOT PICK **",
                "SUBURB": "** DO NOT PICK **",
                "POSTCODE": null,
                "COUNTRY": "** DO NOT PICK **",
                "PHONE": null,
                "MOBILE": null
            },
            "CONTACT": {
                "PHONE": null,
                "MOBILE": null,
                "EMAIL": null
            },
            "TERRITORY": {
                "ID": "SN",
                "NAME": "Sundry"
            },
            "TYPE": {
                "ID": "WS",
                "NAME": "Standard Wholesale"
            }
        }
    }
}
```

---

# Endpoint: Suppliers

<a name="endpoint-suppliers-index"></a>
## Index

Returns an index of basic data about matching supplier master data. Can be
filtered using parameters to identify a subset of suppliers. Use the returned
`ID` field to retrieve detailed data using the `Detail` endpoint.

* URL: `/v1/suppliers`
* Method: `GET`

### Parameters

The following GET parameters are supported:

* Optional: `type`
    * String[3]
    * Supplier Type ('CS' System Table)
* Optional: `group`
    * String[3]
    * Supplier Group ('RV' System Table)
* Optional: `status`
    * String[1]
    * Suppier Account Status

### Response

Example response to `GET /v1/suppliers?group=svs`

```json
{
    "COUNT": 3,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "ID": "COLBRU",
            "NAME": "Colom Bruce"
        },
        {
            "ID": "GITLAB",
            "NAME": "GitLab Services"
        },
        {
            "ID": "EASTEN",
            "NAME": "Eastern Energy"
        }
    }
}

```

<a name="endpoint-suppliers-header"></a>
## Header

Returns detailed supplier master data.

* URL: `/v1/suppliers/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the supplier to retrieve full data for.

### Response

Example response to `GET /v1/suppliers/sundrycr`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "SUNDRYCR": {
            "SUPPLIER_NAME": "Sundry Creditor",
            "STATUS": {
                "ID": null,
                "DESCRIPTION": "OK"
            },
            "CURRENCY": null,
            "TERMS_DAYS": 0,
            "ADDRESS": {
                "LINE1": null,
                "LINE2": null,
                "SUBURB": "Melbourne  Vic",
                "POSTCODE": "3156"
            },
            "CONTACT": {
                "NAME": null,
                "PHONE": null,
                "MOBILE": null,
                "EMAIL": "mail@example.com"
            },
            "DATE": {
                "LAST_CHANGED": null,
                "LAST_PURCHASE": "2019-09-16"
            },
            "TYPE": "1SV",
            "GROUP": "SVA"
        }
    }
}
```

---

# Endpoint: Sales Orders

<a name="endpoint-sorders-index"></a>
## Index

Returns an index of sales orders. Can be filtered using parameters to identify
a subset of sales orders. Use the returned `ID` field to retrieve detailed data
using the `Detail` endpoint.

* URL: `/v1/salesorders`
* Method: `GET`

### Parameters

The following GET parameters are supported:

* Optional: `customer`
    * String
    * Customer ID
* Optional: `custref`
    * String
    * Customer Reference
* Optional: `territory`
    * String[2]
    * Territory Code
* Optional: `status`
    * Integer
    * Order Status Code

### Response

Example response to `GET /v1/salesorders?status=18`

```json
{
    "COUNT": 2,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "ID": "64495",
            "CUSTOMER_ID": "JBQLD",
            "CUSTOMER_REFERENCE": "400051234",
            "STATUS": "18"
        },
        {
            "ID": "64496",
            "CUSTOMER_ID": "JBVIC",
            "CUSTOMER_REFERENCE": "400005678",
            "STATUS": "18"
        }
    }
}

```

<a name="endpoint-sorders-header"></a>
## Header

Returns detailed sales order data.

* URL: `/v1/salesorders/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the sales order to retrieve full data for.

### Response

Example response to `GET /v1/salesorders/66062`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "66062": {
            "WAREHOUSE": "FTGF",
            "CUST_REFERENCE": "4000053227",
            "CONSIGNMENT_NUMBER": null,
            "KEYED_BY": "joe.smith",
            "CUSTOMER": {
                "CHILD": {
                    "ID": "JBVIC",
                    "NAME": "Johnson Brothers Vic DC"
                },
                "PARENT": {
                    "ID": "JBHO",
                    "NAME": "Johnson Brothers Head Office"
                }
            },
            "ORDER_STATUS": {
                "ID": "18",
                "DESCRIPTION": "FORWARD ORDER"
            },
            "DATES": {
                "ORDERED": "2019-09-19",
                "DELIVERY": "2019-11-14"
            },
            "DELIVERY_ADDRESS": null,
            "DELIVERY_INSTRUCTIONS": null
        }
    }
}
```

<a name="endpoint-sorders-lines"></a>
## Lines

Returns sales order lines for given ID.

* URL: `/v1/salesorders/:pk/lines`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the sales order to retrieve full data for.

### Response

Example response to `GET /v1/salesorders/66062/lines`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "1": {
            "IS_STOCK_ITEM": true,
            "LINE_ITEM": "FM431003",
            "LINE_DESCRIPTION": "EC Dishwash Liq 1Ltr",
            "PICK_REFERENCE": null,
            "LINE_TYPE": {
                "ID": "SN",
                "DESCRIPTION": "Normal"
            },
            "QUANTITY": {
                "ORDERED": 288,
                "SHIPPED": 0,
                "PICKED": 0,
                "UOM": "CT09"
            },
            "SHIPPING_UNITS": {
                "PALLETS": 3,
                "LAYERS": 0,
                "CARTONS": 0
            }
        },
        "2": {
            "IS_STOCK_ITEM": true,
            "LINE_ITEM": "FM431005",
            "LINE_DESCRIPTION": "EC Laundry Liq ltr",
            "PICK_REFERENCE": null,
            "LINE_TYPE": {
                "ID": "SN",
                "DESCRIPTION": "Normal"
            },
            "QUANTITY": {
                "ORDERED": 192,
                "SHIPPED": 0,
                "PICKED": 0,
                "UOM": "CT09"
            },
            "SHIPPING_UNITS": {
                "PALLETS": 2,
                "LAYERS": 0,
                "CARTONS": 0
            }
        }
    }
}
```

<a name="endpoint-sorders-packages"></a>
## Packages

Returns packages for given sales order ID. These are results of scan-pack data
so will not return data for orders in status < 40 or if warehouse staff have not
yet started to pick and scan the order.

Important: There is *not* a 1:1 relationship between sales order lines and sales
order packages.

* URL: `/v1/salesorders/:pk/packages`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the sales order to retrieve full data for.

### Response

Key of the object array is the SSCC barcode number for each package.

Example response to `GET /v1/salesorders/66068/packages`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "123406920003711139": {
            "ORDER_NUM": "66068",
            "ORDER_SUF": null,
            "LINE_SEQ": 1,
            "PACKAGE_QTY": 48,
            "PRODUCT": {
                "ID": "FM123456",
                "DESCRIPTION": "EC Dishwash Liq 1Ltr"
            }
        },
        "123406920003711092": {
            "ORDER_NUM": "66068",
            "ORDER_SUF": null,
            "LINE_SEQ": 2,
            "PACKAGE_QTY": 64,
            "PRODUCT": {
                "ID": "FM654321",
                "DESCRIPTION": "Purist Conc 4Lt"
            }
        },
        "123406920003711122": {
            "ORDER_NUM": "66068",
            "ORDER_SUF": null,
            "LINE_SEQ": 3,
            "PACKAGE_QTY": 60,
            "PRODUCT": {
                "ID": "FM342313",
                "DESCRIPTION": "OC Moisture 725ml"
            }
        }
    }
}
```

# Endpoint: Purchase Orders

<a name="endpoint-porders-index"></a>
## Index

Returns an index of purchase orders. Can be filtered using parameters to identify
a subset of purchase orders. Use the returned `ID` field to retrieve detailed data
using the `Detail` endpoint.

* URL: `/v1/purchaseorders`
* Method: `GET`

### Parameters

The following GET parameters are supported:

* Optional: `supplier`
    * String
    * Supplier ID
* Optional: `status`
    * Integer
    * Order Status Code

### Response

Example response to `GET /v1/purchaseorders?supplier=acme&status=40`

```json
{
    "COUNT": 2,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "ID": "10625",
            "CREDITOR_ID": "ACME",
            "STATUS": "40"
        },
        {
            "ID": "10698",
            "CREDITOR_ID": "ACME",
            "STATUS": "40"
        }
    }
}

```

<a name="endpoint-porders-header"></a>
## Header

Returns detailed purchase order data.

* URL: `/v1/purchaseorders/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the purchase order to retrieve full data for.

### Response

Example response to `GET /v1/purchaseorders/10625`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "10625": {
            "INWARDS_NO": null,
            "PO_WHSE_CODE": "FTGF",
            "INVOICE_NO": null,
            "VESSEL_NAME": null,
            "ENTRY_PORT": null,
            "VOYAGE_NO": null,
            "LC_NUMBER": null,
            "SHIPMENT_NO": null,
            "KEYED_BY": "joe.smith",
            "PO_REVISION_NO": "1",
            "PO_ORDER_TYPE": "P",
            "RECEIVED_BY": null,
            "PAY_BY_DATE": "2019-10-15",
            "INVOICE_DUE_DATE": "2019-09-06",
            "SUPPLIER": {
                "ID": "ACME",
                "NAME": "Acme Incorporated"
            },
            "STATUS": {
                "ID": "40",
                "DESCRIPTION": "On Order"
            },
            "DATES": {
                "ORDER": "2019-09-06",
                "ARRIVAL": "2019-09-06",
                "RECEIPT": null
            },
            "DELIVERY_INSTRUCTIONS": [
                "*** Deliver to RECEPTION ***"
            ]
        }
    }
}
```

<a name="endpoint-porders-lines"></a>
## Lines

Returns purchase order lines for given ID.

* URL: `/v1/purchaseorders/:pk/lines`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the purchase order to retrieve full data for.

### Response

Example response to `GET /v1/purchaseorders/10625/lines`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "1": {
            "IS_STOCK_ITEM": true,
            "STOCK_CODE": "TB0001",
            "SUPPLIER_STOCK_CODE": null,
            "LINE_DESCRIPTION": "Product Description TB0001",
            "DATE_EXPECTED": "2019-09-05",
            "LINE_TYPE": {
                "ID": "SN",
                "DESCRIPTION": "Normal Line"
            },
            "QUANTITIES": {
                "ORDERED": 12,
                "RECEIVED": 0,
                "BACKORDERED": 0,
                "UOM": "1000",
                "CONVERSION": 1
            }
        },
        "2": {
            "IS_STOCK_ITEM": false,
            "STOCK_CODE": "Special Item",
            "SUPPLIER_STOCK_CODE": "20N4S01M00",
            "LINE_DESCRIPTION": "Road Runner Explosives",
            "DATE_EXPECTED": "2019-09-06",
            "LINE_TYPE": {
                "ID": "SS",
                "DESCRIPTION": "Special Line"
            },
            "QUANTITIES": {
                "ORDERED": 1,
                "RECEIVED": 0,
                "BACKORDERED": 0,
                "UOM": "EACH",
                "CONVERSION": 1
            }
        },
        "3": {
            "IS_STOCK_ITEM": false,
            "STOCK_CODE": null,
            "SUPPLIER_STOCK_CODE": null,
            "LINE_DESCRIPTION": "As per quote 123456",
            "DATE_EXPECTED": "2019-09-06",
            "LINE_TYPE": {
                "ID": "DN",
                "DESCRIPTION": "Note Line"
            }
        },
        "4": {
            "IS_STOCK_ITEM": false,
            "STOCK_CODE": null,
            "SUPPLIER_STOCK_CODE": null,
            "LINE_DESCRIPTION": "This is a memo line visible only internally.",
            "DATE_EXPECTED": "2019-09-06",
            "LINE_TYPE": {
                "ID": "DM",
                "DESCRIPTION": "Memo Line"
            }
        }
    }
}
```

---

# Endpoint: Work Orders

<a name="endpoint-worders-index"></a>
## Index

Returns an index of work orders. Can be filtered using parameters to identify
a subset of work orders. Use the returned `ID` field to retrieve detailed
data using the `Detail` endpoint.

* URL: `/v1/workorders`
* Method: `GET`

### Parameters

The following GET parameters are supported:

* Optional: `product`
    * String
    * Product ID
* Optional: `warehouse`
    * String
    * ID of Output Warehouse
* Optional: `status`
    * Integer
    * Work Order Status Code

### Response

Example response to `GET /v1/workorders?status=10&product=FM565603`

```json
{
    "COUNT": 2,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "WORK_ORDER": "29526",
            "PRODUCT_ID": "FM565603",
            "STATUS_ID": "10"
        },
        {
            "WORK_ORDER": "29527",
            "PRODUCT_ID": "FM565603",
            "STATUS_ID": "10"
        }
    }
}

```

<a name="endpoint-worders-header"></a>
## Header

Returns detailed work order data.

* URL: `/v1/workorders/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the work order to retrieve full data for.

### Response

Example response to `GET /v1/workorders/29526`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        "29526": {
            "DOCUMENT_TYPE": null,
            "ORDER_CODE": null,
            "DOCUMENT_SUFFIX": null,
            "WO_USER_NAME": "joe.smith",
            "BOM_ID": "75",
            "PRODUCT": {
                "ID": "FM565603",
                "DESCRIPTION": "EC Dishwash Liq 1Ltr"
            },
            "STATUS": {
                "ID": "10",
                "DESCRIPTION": "Committed"
            },
            "WAREHOUSE": {
                "OUTPUT": "FTGF",
                "FACTORY": "FTGM"
            },
            "QUANTITIES": {
                "ORDERED": 31693,
                "COMPLETED": 0,
                "REJECTED": 0,
                "QC": 0,
                "UPDATED": 0
            },
            "DATES": {
                "CREATED": "2019-09-17",
                "ACTUAL_START": null,
                "ACTUAL_FINISH": null,
                "EXPECTED_START": "2019-10-01",
                "EXPECTED_FINISH": "2019-10-03",
                "EXPECTED_DURATION": 16,
                "LAST_CHANGE": null
            }
        }
    }
}
```

<a name="endpoint-worders-lines"></a>
## Lines

Returns work order lines for given ID.

* URL: `/v1/workorders/:pk/lines`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the work order to retrieve full data for.

### Response

Example response to `GET /v1/workorders/29526/lines`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "TRN_CODE": "COM",
            "SEQ": 4,
            "COMP_CODE": "LA141003B",
            "COMP_DESC": "EC Lemon Dishwash 1ltr BCK",
            "DATE_REQUIRED": null,
            "TRN_DATE": "2019-09-17",
            "TRN_WHSE": "FTGR",
            "TRN_QTY": 289.5156,
            "UPDATE_FLAG": "C",
            "TRAN_BATCH_REF": null
        },
        {
            "TRN_CODE": "QPER",
            "SEQ": 4,
            "COMP_CODE": "LA141003B",
            "COMP_DESC": "EC Lemon Dishwash 1ltr BCK",
            "DATE_REQUIRED": null,
            "TRN_DATE": "2019-09-17",
            "TRN_WHSE": "FTGR",
            "TRN_QTY": 0.0091,
            "UPDATE_FLAG": "C",
            "TRAN_BATCH_REF": null
        },
        {
            "TRN_CODE": "COM",
            "SEQ": 8,
            "COMP_CODE": "BT1002",
            "COMP_DESC": "EC Clr 1Ltr BTL",
            "DATE_REQUIRED": null,
            "TRN_DATE": "2019-09-17",
            "TRN_WHSE": "FTGR",
            "TRN_QTY": 285.237,
            "UPDATE_FLAG": "C",
            "TRAN_BATCH_REF": null
        }
    }
}
```

<a name="endpoint-worders-dockets"></a>
## Dockets

Returns production entry dockets for given work order ID.

* URL: `/v1/workorders/:pk/dockets`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the work order to retrieve dockets for.

### Response

Example response to `GET /v1/workorders/66068/dockets`:

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-19T16:24:50+10:00",
    "RESULTS": {
        {
            "PROD_ENTRY_TYPE": "P",
            "PROD_ENTRY_SHIFT": "1",
            "ENTRY_STATUS": "U",
            "PROD_ENTRY_QTY": 96,
            "FINISH_FLAG": "W",
            "PROD_ENTRY_REF2": "P",
            "SCREEN_TYPE": "PF",
            "USER_ID": "rf-user1",
            "ENTRY_TIMESTAMP": "2019-09-17T21:01:49+10:00",
            "WAREHOUSE": {
                "SOURCE": "FTGR",
                "OUTPUT": "FTGF"
            }
        },
        {
            "PROD_ENTRY_TYPE": "P",
            "PROD_ENTRY_SHIFT": "1",
            "ENTRY_STATUS": "U",
            "PROD_ENTRY_QTY": 24,
            "FINISH_FLAG": "W",
            "PROD_ENTRY_REF2": "P",
            "SCREEN_TYPE": "PF",
            "USER_ID": "rf-user2",
            "ENTRY_TIMESTAMP": "2019-09-17T21:12:03+10:00",
            "WAREHOUSE": {
                "SOURCE": "FTGR",
                "OUTPUT": "FTGF"
            }
        },
        {
            "PROD_ENTRY_TYPE": "P",
            "PROD_ENTRY_SHIFT": "D",
            "ENTRY_STATUS": "U",
            "PROD_ENTRY_QTY": 192,
            "FINISH_FLAG": "W",
            "PROD_ENTRY_REF2": null,
            "SCREEN_TYPE": "P",
            "USER_ID": "joe.smith",
            "ENTRY_TIMESTAMP": "2019-09-18T07:34:52+10:00",
            "WAREHOUSE": {
                "SOURCE": "FTGR",
                "OUTPUT": "FTGF"
            }
        }
    }
}
```

---

<a name="endpoint-system-code-tables"></a>
# Endpoint: System Code Tables

Code Tables are exposed via the `/v1/sys` endpoint.

* URL: `/v1/sys/:pk`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the code table to retrieve.

The following tables are exposed (these are standard Pronto system tables):

* `B6`: Product Brands
* `CA`: Carriers
* `CN`: Credit Note Reasons
* `CS`: Supplier Type
* `CT`: Customer Types
* `CU`: Foreign Currencies
* `RV`: Supplier Groups
* `SQ`: Short Ship Reasons
* `SU`: Shelf Life Codes
* `SX`: Product Sort Code
* `SZ`: Product Holding Type
* `TC`: Customer Territories
* `UM`: Units of Measure
* `UN`: Chemical UN Numbers
* `WH`: Warehouses

### Responses

Various formats depending on table definition.

## Key Lookup

* URL: `/v1/sys/:pk/:key`
* Method: `GET`

### Parameters

* Required: `:pk` the ID of the code table to retrieve.
* Required: `:key` key to lookup in the code table.

### Examples

Lookup key '14' in the 'B6' (Brand) table: `/v1/sys/b6/14`

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-20T13:02:17+10:00",
    "RESULTS": {
        "14": {
            "DESCRIPTION": "Ear Chimes"
        }
    }
}
```

Lookup key 'TOLL' in the 'CA' (carriers) table: `/v1/sys/ca/toll`

```json
{
    "COUNT": 1,
    "SERVER": "apiserver.example.com",
    "DB": "l01@pronto.example.com",
    "TZ": "2019-09-20T13:03:46+10:00",
    "RESULTS": {
        "TOLL": {
            "DESCRIPTION": "Toll Freight Services"
        }
    }
}
```
