<?php

Class Authentication {

  const MIN_KEY_LENGTH=16;
  private $user_keys = array();

  function __construct() {
    $f3=Base::instance();
    $apikeys = $f3->get('apikeys');

    if (is_array($apikeys)) {
      foreach ($apikeys as $key => $value) {
        $key_name = $apikeys[$key]['name'];
        $key_db   = $apikeys[$key]['db'];
        if (!$key_name) unset($apikeys[$key]);
        if (!$key_db)   unset($apikeys[$key]);
        if (strlen($key) < $this::MIN_KEY_LENGTH)
          $f3->error(500, sprintf("CONFIG ERROR: Key %s too short. Minimum length is %s.", $key, $this::MIN_KEY_LENGTH));
      }
    }

    // create a 'default' api key for IP bypass use. set the associated
    // database to the first db listed in the 'valid_dbs' directive from
    // the configuration file
    $apikeys['default'] = array(
      'name' => 'IP Bypass',
      'db'   => $f3->get('odbc.valid_dbs')[0],
    );

    $this->user_keys = $apikeys;
    return true;
  }

  function key_info($key) {
    return $this->user_keys[$key];
  }

  /*
   * validate user access token
   */
  function authenticate_client() {
    $f3=Base::instance();
    $ipcompare = new IPcompare;
    
    $client_key = $f3->get('REQUEST.apikey') ?: null;
    if (!$client_key)
      $client_key = $f3->get('HEADERS.X-Apikey') ?: null;
    $client_ip = $f3->get('IP');
    $auth_bypass_ips = $f3->get('auth_bypass_ips');

    // check if source address of client bypasses apikey requirement
    if (!$client_key && is_array($auth_bypass_ips))
      foreach ( $auth_bypass_ips as $cidr)
        if ($ipcompare->ipv4_in_range($client_ip, $cidr) or $ipcompare->ipv6_in_range($client_ip, $cidr) ) {
          $this->log_auth(sprintf('Client IP %s is in auth bypass list; Set default api key and bypassing key checks.', $client_ip));
          $f3->set('apikey', 'default');
          return true;
        }

    // 401 Unauthorized if there's no api token
    if (!$client_key)
      $this->fail_auth(401); // auth was not even attempted

    // validate token
    if (strlen($client_key) < $this::MIN_KEY_LENGTH or !array_key_exists($client_key, $this->user_keys))
      $this->fail_auth(403); // auth attempted but failed

    // log successful authentication
    $f3->set('apikey', $client_key);
    $this->log_auth(sprintf('Authenticated client "%s" (Key: %s)',
      $this->key_info($client_key)['name'], $client_key));

    return true;
  }

  private function fail_auth($err) {
    $f3=Base::instance();
    $this->log_auth('Client failed authentication. Error: '.$err);
    $f3->error($err);
  }

  private function log_auth($msg) {
    $logger = new Log('auth.log');
    $logger->write($msg);
    return true;
  }


}
