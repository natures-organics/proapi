<?php

namespace BOM;

class Index extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $where_product  = $f3->get('REQUEST.product') ?: null;
    $where_type     = $f3->get('REQUEST.type') ?: null;

    $sql = "
      SELECT  bomh.bomh_id              AS ID,
              bomh.bomh_description     AS DESCRIPTION,
              TRIM(bomh.bomh_type)      AS TYPE,
              TRIM(bomh.stock_code)     AS STOCK_ID
      FROM bill_of_materials_header AS bomh
      WHERE 1=1";
    $args = array();
    if ( $where_product ) {
      $sql .= ' AND (UPPER(TRIM(bomh.stock_code)) = UPPER(?))';
      $args[] = $where_product;
    }
    if ( $where_type ) {
      $sql .= ' AND (UPPER(bomh.bomh_type) = UPPER(?))';
      $args[] = $where_type;
    }
    $res = $odbc->query($sql, $args);
    $this->return_data2client($res);
  }

}
