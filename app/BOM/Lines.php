<?php

namespace BOM;

class Lines extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $res_headers = null;
    $id = $params['bomid'];

    $sql = "
      SELECT  bomd.bom_seq_no           AS BOM_SEQ_NO,
              TRIM(bomd.bom_line_type)  AS LINE_TYPE,
              TRIM(sm.stock_code)       AS STK_CODE,
              TRIM(sm.stk_description)  AS STK_DESC,
              bomd.bom_quantity         AS BOM_QUANTITY,
              TRIM(bomd.bom_unit_desc)  AS BOM_UNIT_DESC
              --bomd.wastage_per_cent,
              --bomd.bom_x_quantity,
              --bomd.bom_y_quantity,
              --bomd.bom_plan_percent,
              --bomd.line_bom_id_linked,
              --bomd.comp_bom_defined,
              --bomd.bomd_formula,
              --bomd.bomd_wastage_formula,
              --bomd.instruction_formula,
              --bomd.comp_weight_formula,
              --bomd.uom_formula
      FROM bill_of_materials_detail AS bomd
      JOIN bill_of_materials_header AS bomh ON (bomd.bomh_id = bomh.bomh_id)
      LEFT JOIN stock_master AS sm ON (bomd.comp_code = sm.stock_code)
      WHERE bomh.bomh_id = ?";
    $args[] = $id;
    $res_headers = $odbc->query($sql, $args);

    $data = $this->massage_arrays($res_headers);
    $this->return_data2client($data);
  }

  private function massage_arrays($headers) {
    foreach ($headers as $row) {
      $row_id = floatval($row['BOM_SEQ_NO']);
      $row['PRODUCT'] = array(
        'ID'          => $row['STK_CODE'],
        'DESCRIPTION' => $row['STK_DESC'],
      );
      $row['QUANTITY'] = array(
        'BOM' => floatval($row['BOM_QUANTITY']),
        'UOM' => $row['BOM_UNIT_DESC'],
      );
      unset($row['BOM_SEQ_NO'],
        $row['STK_CODE'], $row['STK_DESC'],
        $row['BOM_QUANTITY'], $row['BOM_UNIT_DESC']
      );
      $results[$row_id] = $row;
    }

    return $results;
  }

}
