<?php

namespace BOM;

class Main extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['bomid'];

    $sql = "
      SELECT  bomh.bomh_id              AS BOM_ID,
              bomh.bomh_description     AS BOM_DESCRIPTION,
              TRIM(bomh.bomh_type)      AS BOM_TYPE_ID,
              CASE
                WHEN TRIM(bomh.bomh_type) = 'P' THEN 'PRIMARY'
                WHEN TRIM(bomh.bomh_type) = 'N' THEN 'ALTERNATE'
                ELSE TRIM(bomh.bomh_type)
              END AS BOM_TYPE_DESC,
              TRIM(bomh.stock_code)     AS STK_CODE,
              TRIM(sm.stk_description)  AS STK_DESC,
              TRIM(bomh.item_revision)  AS REVISION,
              TRIM(bomh.reference)      AS REFERENCE,
              bomh.date_last_change     AS DATE_LAST_CHANGE,
              bomh.bom_start_date       AS DATE_START,
              bomh.end_date             AS DATE_END,
              TRIM(bomh.bom_comment)    AS BOM_COMMENT,
              bomh.formulation_qty      AS FORMULATION_QTY,
              bomh.authorised_flag      AS AUTHORIZED_FLAG,
              bomh.bom_yield/100        AS YIELD_PCENT
      FROM bill_of_materials_header AS bomh
      LEFT JOIN stock_master AS sm ON (bomh.stock_code = sm.stock_code)
      WHERE (bomh.bomh_id = ?)";
    $args = array($id);
    $res = $odbc->query($sql, $args);
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  private function massage_arrays($headers) {
    foreach ($headers as $bom) {
      $bom_id = $bom['BOM_ID'];
      $bom['FORMULATION_QTY'] = floatval($bom['FORMULATION_QTY']);
      $bom['YIELD_PCENT'] = floatval($bom['YIELD_PCENT']);
      $bom['AUTHORIZED_FLAG'] = $this->convert_string_to_boolean($bom['AUTHORIZED_FLAG']);
      $bom['PRODUCT'] = array(
        'ID'          => $bom['STK_CODE'],
        'DESCRIPTION' => $bom['STK_DESC'],
      );
      $bom['TYPE'] = array(
        'ID'          => $bom['BOM_TYPE_ID'],
        'DESCRIPTION' => $bom['BOM_TYPE_DESC'],
      );
      unset($bom['BOM_TYPE_ID'], $bom['BOM_TYPE_DESC']);
      $bom['DATES'] = array(
        'START'       => $bom['DATE_START'] == '1899-12-31' ? null : $this->format_date($bom['DATE_START']),
        'END'         => $bom['DATE_END'] == '1899-12-31' ? null : $this->format_date($bom['DATE_END']),
        'LAST_CHANGE' => $bom['DATE_LAST_CHANGE'] == '1899-12-31' ? null : $this->format_date($bom['DATE_LAST_CHANGE']),
      );
      unset($bom['BOM_ID'],
        $bom['STK_CODE'], $bom['STK_DESC'],
        $bom['DATE_START'], $bom['DATE_END'], $bom['DATE_LAST_CHANGE']
      );
      $results[$bom_id] = $bom;
    }

    return $results;
  }

}
