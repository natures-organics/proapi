<?php

class Controller extends \Prefab {

  const ISO8601_date = 'Y-m-d';
  const ISO8601_timestamp = 'Y-m-d\TH:i:sP';
  const VERSION = '1.3';

  function beforeRoute($f3,$params) {
    1;
  }

  function afterRoute($f3,$params) {
    1;
  }

  /*
   * takes an array from a module, does some general global validation
   * then passes it to the appropriate function for the clients requested
   * response format (ie json or xml)
   */
  function return_data2client($data) {
    $f3=Base::instance();
    
    if (count($data) > $f3->get('max_results'))
      $f3->error(413); // payload too large
    if (count($data) == 0 )
      $f3->error(204); // success; no content
    
    $client_accepts = explode(',',
      $f3->get('REQUEST.format') ?: $f3->get('HEADERS.Accept') ?: $f3->get('default_format'));
    $format = null;
    foreach($client_accepts as $f) {
      $mime_type = explode('/', $f)[0];
      $mime_subtype = explode('/', $f)[1];

      // if the client will accept anything, then return json
      if ($mime_type == '*' and $mime_subtype == '*') {
        $format = 'json';
        break;
      }
      if ($mime_subtype == 'json') {
        $format = 'json';
        break;
      }
      if ($mime_subtype == 'xml') {
        $format = 'xml';
        break;
      }
    }

    switch ($format) {
    case 'xml':
      $this->output_xml($data);
      break;
    case 'json':
      $this->output_json($data);
      break;
    default:
      $this->log_info(sprintf('Unknown data format: "%s"', $format));
      $f3->error(406);  // 'Not Acceptable'
      break;
    }
    $f3->set('REQUEST.format', $format);
  }

  /*
   * given a data structure in an associative array, print out a json
   * representation of the data.
   */
  function output_json($data) {
    $f3=Base::instance();
    $odbc=ODBC::instance();
    
    $data = $this->convert_blank_to_null($data);
    $data = $this->add_href_links($data);
    $output = array(
      'COUNT'   => count($data),
      'SERVER'  => gethostname(),
      'DB'      => sprintf('%s@%s', $odbc->get_dbname(), $odbc->get_host()),
      'TZ'      => date(self::ISO8601_timestamp),
      'RESULTS' => $data,
    );

    header('Content-Type: application/json;charset=utf-8');
    $json = json_encode($output, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
    if (!$json) {
      $this->log_error(json_last_error_msg());
      $f3->error(500);
    }
    print($json);
  }

  /*
   * given a data structure in an associative array, print out an xml
   * representation of the data.
   */
  function output_xml(array $data) {
    $odbc=ODBC::instance();

    $output['META'] = array(
      'COUNT'   => count($data),
      'SERVER'  => gethostname(),
      'DB'      => array(
        'HOST'  => $odbc->get_host(),
        'NAME'  => $odbc->get_dbname(),
      ),
      'TZ'      => date(self::ISO8601_timestamp),
    );
    $output['RESULTS'] = $data;
    $simpleXml = new \SimpleXMLElement('<root/>');
    $this->array2xml($simpleXml, $output);
    print($simpleXml->asXML());
  }
  function array2xml(SimpleXMLElement $object, array $data) {
    foreach ($data as $key => $value) {
      if (is_numeric($key))
        $key = "RECORD_$key";
      if (is_array($value)) {
        $new_object = $object->addChild($key);
        $this->array2xml($new_object, $value);
      } else {
        $object->addChild($key, htmlspecialchars($value));
      }
    }
  }
  
  /*
   * logging functions
   */
  function log_info($msg) {
    return $this->logit($msg);
  }
  function log_error($msg) {
    return $this->logit($msg, 'error.log');
  }
  function logit($msg, $fname = 'info.log') {
    if (!$msg) return null;
    $logger = new Log($fname);
    $logger->write($msg);
    return true;
  }

  /*
   * validate a string to see if it matches a known sales order number pattern
   */
  function is_valid_so_number($str) {
    return (preg_match('/^[\d]+[A-Z]*$/i', $str) == 1);
  }

  /*
   * iterate over elements of an array and convert blank strings to null
   */
  function convert_blank_to_null($array) {
    if (! is_array($array)) return $array;

    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $array[$key] = $this->convert_blank_to_null($value);
        continue;
      }

      if ($value === '')
        $array[$key] = null;
    }
    return $array;
  }
  
  /*
   * convert a string representation of 'true' (eg, 1, 'Yes' etc) to boolean true
   * otherwise return boolean false
   */
  function convert_string_to_boolean($str) {
    if ($str === true) 
      return true;
    if ($str == '1') 
      return true;
    if (strtolower($str) == 'yes')
      return true;
    if (strtolower($str) == 'y')
      return true;
    if (strtolower($str) == 'true')
      return true;

    // default to false
    return false;
  }

  /*
   * format string into well-formed date
   */
  function format_date($str) {
    if (!$str) return null;
    if (($timestamp = strtotime($str)) === false)
      // php could not interpret the given $str as a date
      return null;

    return date(self::ISO8601_date, $timestamp);
  }
  
  /*
   * format string into well-formed timestamp, converting from UTC to local time
   */
  function format_timestamp($str) {
    $f3=Base::instance();

    if (!$str) return null;
    
    // create a $dt object with the UTC timezone
    $dt = new DateTime($str, new DateTimeZone('UTC'));

    // change the timezone of the object without changing it's time
    $dt->setTimezone(new DateTimeZone($f3->get('timezone')));

    // format the datetime
    return $dt->format(self::ISO8601_timestamp);
  }


  /*
   * add href links to relevant api endpoints to the returned data
   */
  private function add_href_links($data) {
    $f3=Base::instance();

    if (! is_array($data)) return $data;

    foreach ($data as &$record) {
      if (array_key_exists('ID', $record)) {
        // link to it!
        $record['HREF'] = sprintf('%s://%s%s/%s',
          $f3->get('SCHEME'),
          $f3->get('HOST'),
          $f3->get('PATH'),
          $record['ID']
        );
      }
    }
    return $data;
  }

}
