<?php

namespace Customers;

class Index extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $where_terr   = $f3->get('REQUEST.territory') ?: null;
    $where_type   = $f3->get('REQUEST.type') ?: null;
    $where_status = $f3->get('REQUEST.status') ?: null;

    $sql = "
      SELECT
        TRIM(dm.accountcode)          AS ID, 
        TRIM(dm.shortname)            AS NAME,
        UPPER(TRIM(dm.territory))     AS TERRITORY,
        UPPER(TRIM(dm.dr_cust_type))  AS TYPE,
        UPPER(TRIM(dm.deb_status))    AS STATUS
      FROM deb_master AS dm
      WHERE 1=1\n";

    // optionally append WHERE clause(s) to sql query
    $args = array();
    if ( $where_terr ) {
      $sql .= ' AND (UPPER(TRIM(dm.territory)) = UPPER(?))';
      $args[] = $where_terr;
    }
    if ( $where_type ) {
      $sql .= ' AND (UPPER(TRIM(dm.dr_cust_type)) = UPPER(?))';
      $args[] = $where_type;
    }
    if ( $where_status ) {
      $sql .= ' AND (UPPER(TRIM(dm.deb_status)) = UPPER(?))';
      $args[] = $where_status;
    }

    // execute query, reformat results and send to the client
    $sql .= "\nORDER BY dm.accountcode";
    $res = $odbc->query($sql, $args);
    $this->return_data2client($res);
  }

}
