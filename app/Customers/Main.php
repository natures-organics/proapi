<?php

namespace Customers;

class Main extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['custid'];

    $sql = "
      SELECT
        TRIM(dm.accountcode)          AS CUSTCODE, 
        TRIM(dm.shortname)            AS SHORTNAME,
        TRIM(dm.deb_status)           AS STATUS,
        TRIM(dm_parent.accountcode)   AS PARENTCODE,
        TRIM(dm_parent.shortname)     AS PARENTNAME,
        TRIM(nam_email.na_name||nam_email.na_company) AS EMAIL,
        -- address data
        TRIM(nam_address.na_name)     AS CUSTNAME,
        TRIM(nam_address.na_company)  AS ADDR1,
        TRIM(nam_address.na_street)   AS ADDR2,
        TRIM(nam_address.na_suburb)   AS SUBURB,
        TRIM(nam_address.na_country)  AS COUNTRY,
        TRIM(nam_address.postcode)    AS POSTCODE,
        TRIM(nam_address.na_phone)    AS PHONE,
        TRIM(nam_address.na_phone_2)  AS MOBILE,
        -- delivery address
        TRIM(nam_da.na_name)    AS DELIVERY_NAME,
        TRIM(nam_da.na_company) AS DELIVERY_ADDR1,
        TRIM(nam_da.na_street)  AS DELIVERY_ADDR2,
        TRIM(nam_da.na_suburb)  AS DELIVERY_SUBURB,
        TRIM(nam_da.na_country) AS DELIVERY_COUNTRY,
        TRIM(nam_da.postcode)   AS DELIVERY_POSTCODE,
        TRIM(nam_da.na_phone)   AS DELIVERY_PHONE,
        TRIM(nam_da.na_phone_2) AS DELIVERY_MOBILE,
        -- meta data
        TRIM(dm.territory)            AS TERRITORY_ID,
        TRIM(tc.sys_description)      AS TERRITORY_DESC,
        TRIM(dm.dr_cust_type)         AS CUSTTYPE_ID,
        TRIM(ct.sys_description)      AS CUSTTYPE_DESC,
        TRIM(dm.route_code)           AS CARRIER_ID
      FROM deb_master AS dm
      LEFT JOIN deb_master AS dm_parent ON (dm.bill_to = dm_parent.accountcode)
      LEFT JOIN name_and_address_master AS nam_address ON (dm.accountcode = nam_address.accountcode and nam_address.na_type = 'C')
      LEFT JOIN name_and_address_master AS nam_da ON (dm.accountcode = nam_da.accountcode and nam_da.na_type = 'DA')
      LEFT JOIN name_and_address_master AS nam_di ON (dm.accountcode = nam_di.accountcode and nam_di.na_type = 'DI')
      LEFT JOIN name_and_address_master AS nam_email ON (dm.accountcode = nam_email.accountcode and nam_email.na_type = 'E')
      LEFT JOIN system_table AS tc ON (tc.sys_tbl_type = 'TC' AND tc.sys_tbl_code = dm.territory)
      LEFT JOIN system_table AS ct ON (ct.sys_tbl_type = 'CT' AND ct.sys_tbl_code = dm.dr_cust_type)
      LEFT JOIN rep_master AS rm ON (dm.rep_code = rm.rep_code)
      WHERE UPPER(dm.accountcode) = UPPER(?)\n";

    // execute query, reformat results and send to the client
    $res = $odbc->query($sql, array($id));
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  /****************************************************************************
   * restructures the rows returned from the sql query to be multi-dimensional
   * so the returned json/xml has proper structure
   */
  private function massage_arrays($results) {
    foreach ($results as $row) {
      $id = $row['CUSTCODE'];
      $row['PARENT'] = array(
        'ID' => $row['PARENTCODE'],
        'SHORTNAME' => $row['PARENTNAME'],
      );

      // customer address
      $row['ADDRESS'] = array(
        'LINE1' => $row['ADDR1'],
        'LINE2' => $row['ADDR2'],
        'SUBURB' => $row['SUBURB'],
        'POSTCODE' => $row['POSTCODE'],
        'COUNTRY' => $row['COUNTRY'],
      );
      unset($row['CUSTCODE'], $row['PARENTCODE'], $row['PARENTNAME'],
        $row['ADDR1'], $row['ADDR2'], $row['SUBURB'], $row['POSTCODE'],
        $row['COUNTRY']);

      // delivery address
      if ($row['DELIVERY_ADDR1']) {
        $row['DELIVERY'] = array(
          'NAME' => $row['DELIVERY_NAME'],
          'LINE1' => $row['DELIVERY_ADDR1'],
          'LINE2' => $row['DELIVERY_ADDR2'],
          'SUBURB' => $row['DELIVERY_SUBURB'],
          'POSTCODE' => $row['DELIVERY_POSTCODE'],
          'COUNTRY' => $row['DELIVERY_COUNTRY'],
          'PHONE' => $row['DELIVERY_PHONE'],
          'MOBILE' => $row['DELIVERY_MOBILE'],
        );
      }
      unset($row['DELIVERY_NAME'], $row['DELIVERY_ADDR1'],
        $row['DELIVERY_ADDR2'], $row['DELIVERY_SUBURB'],
        $row['DELIVERY_POSTCODE'], $row['DELIVERY_COUNTRY'],
        $row['DELIVERY_PHONE'], $row['DELIVERY_MOBILE']);

      // contact details
      $row['CONTACT'] = array(
        'PHONE' => $row['PHONE'],
        'MOBILE'=> $row['MOBILE'],
        'EMAIL' => $row['EMAIL'],
      );
      unset($row['PHONE'], $row['MOBILE'], $row['EMAIL']);

      // meta details
      $row['TERRITORY'] = array(
        'ID'    => $row['TERRITORY_ID'],
        'NAME'  => $row['TERRITORY_DESC'],
      );
      unset($row['TERRITORY_ID'], $row['TERRITORY_DESC']);
      $row['TYPE'] = array(
        'ID'    => $row['CUSTTYPE_ID'],
        'NAME'  => $row['CUSTTYPE_DESC'],
      );
      unset($row['CUSTTYPE_ID'], $row['CUSTTYPE_DESC']);

      $data[$id] = $row;
    }

    return $data;
  }
}
