<?php

class ODBC extends \Controller {

  private $dbh = null;  // database handle

  private $dbhost = null;
  private $dbport = null;
  private $dbname = null;
  private $dblocale = null;
  private $dbinstance = null;
  private $cache_timeout = 0;

  /*
   * create a database connection
   */
  function connect($username, $password) {
    if (!$username) return false;
    if (!$password) return false;
    if (!$this->dbhost)     return false;
    if (!$this->dbport)     return false;
    if (!$this->dbname)     return false;
    if (!$this->dbinstance) return false;
    if (!$this->dblocale)   return false;

    $dsn = sprintf('informix:host=%s; service=%s; database=%s; server=%s; protocol=onsoctcp; EnableScrollableCursors=1; client_locale=%s;',
                      $this->dbhost, $this->dbport, $this->dbname, $this->dbinstance, $this->dblocale);
    $this->dbh = new DB\SQL($dsn, $username, $password);
    if ($this->dbh) return true;
    return false;
  }

  /*
   * wrapper for making sql queries to the odbc database
   * allows for global use of caching without having to implement
   * in each controller
   */
  function query($sql, array $params = null) {
    $f3=\Base::instance();
    $results = $this->dbh->exec($sql, $params, $this->cache_timeout);
    if ($f3->get('DEBUG') > 0 )
      $this->logit($this->dbh->log(), 'queries.log');
    return $results;
  }
  
  /*
   * getters
   */
  function get_host() { return $this->dbhost; }
  function get_port() { return $this->dbport; }
  function get_dbname() { return $this->dbname; }
  function get_server() { return $this->dbserver; }
  function get_locale() { return $this->dblocale; }
  function get_cache_timeout() { return $this->cache_timeout; }

  /*
   * setters
   */
  function set_host($str) { return $this->dbhost = $str; }
  function set_port($str) { return $this->dbport = $str; }
  function set_instance($str) { return $this->dbinstance = $str; }
  function set_locale($str) { return $this->dblocale = $str; }
  function set_dbname($str) {
    $f3=\Base::instance();
    // only allow dbnames that are explicitly listed in config file
    $valid_dbs = $f3->get('odbc.valid_dbs');
    if ( ! in_array($str, $valid_dbs) )
      $f3->error(451);
    return $this->dbname = $str;
  }
  function set_cache_timeout($timeout) {
    $f3=\Base::instance();
    if (!is_numeric($timeout)) return false;
    $this->cache_timeout = intval($timeout);
    
    // enable database caching if enabled in config
    if ($f3->get('DEBUG') == 0 && $this->cache_timeout > 0 )
      $f3->set('CACHE', TRUE);

    return $this->cache_timeout;
  }

}
