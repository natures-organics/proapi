<?php

namespace Products;

class Index extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $where_desc   = $f3->get('REQUEST.description') ?: null;
    $where_brand  = $f3->get('REQUEST.brand') ?: null;
    $where_group  = $f3->get('REQUEST.group') ?: null;
    $where_status = $f3->get('REQUEST.status') ?: null;
    $where_cond   = $f3->get('REQUEST.condition') ?: null;
    $where_type   = $f3->get('REQUEST.type') ?: null;

    $sql = "
      SELECT
        TRIM(sm.stock_code)       AS ID,
        TRIM(sm.stk_description)  AS DESCRIPTION,
        sm.condition_code         AS CONDITION,
        TRIM(sm.stk_brand)        AS BRAND_ID,
        TRIM(sm.stock_group)      AS GROUP_ID,
        TRIM(sm.stk_stock_status) AS STATUS_ID,
        TRIM(sm.stk_user_group_2) AS TYPE_ID
      FROM stock_master AS sm
      WHERE 1=1\n";
    $args = array();
    if ( $where_desc ) {
      $sql .= ' AND (UPPER(sm.stk_description) LIKE UPPER(?))';
      $args[] = '%'.$where_desc.'%';
    }
    if ( $where_brand ) {
      $sql .= ' AND (TRIM(sm.stk_brand) = ?)';
      $args[] = $where_brand;
    }
    if ( $where_group ) {
      $sql .= ' AND (TRIM(sm.stock_group) = ?)';
      $args[] = $where_group;
    }
    if ( $where_status ) {
      $sql .= ' AND (UPPER(TRIM(sm.stk_stock_status)) = UPPER(?))';
      $args[] = $where_status;
    }
    if ( isset($where_cond) ) {
      $sql .= ' AND (UPPER(TRIM(sm.condition_code)) = UPPER(?))';
      $args[] = $where_cond;
    }
    if ( $where_type ) {
      $sql .= ' AND (UPPER(TRIM(sm.stk_user_group_2)) = UPPER(?))';
      $args[] = $where_type;
    }

    $sql .= ' ORDER BY sm.stock_code';
    $res = $odbc->query($sql, $args);
    $this->return_data2client($res);
  }

}
