<?php

namespace Products;

class Lots extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['prodid'];
    $whs = $params['whs'] ?: null;

    $sql = "SELECT
      WHERE UPPER(swd.stock_code) = UPPER(?)";
    $args[] = $id;
    if ( $whs ) {
      $sql .= ' AND (UPPER(TRIM(swd.whse_code)) = UPPER(?))';
      $args[] = $whs;
    }
    $sql .= ' ORDER BY swd.whse_code';
    $res = $odbc->query($sql, $args);

    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }
  
  private function massage_arrays($res) {

    $data = array();
    foreach ($res as $row) {
      $id = $row['WHSID'];
      if (floatval($row['AVG_MTH_DEMAND']) > 0) {
        $days_cover = round(((floatval($row['QTY_ON_HAND']) / floatval($row['AVG_MTH_DEMAND'])) * 30), 1);
      } else {
        $days_cover = null;
      }
      $row['QUANTITY'] = array(
        'ON_HAND'         => floatval($row['QTY_ON_HAND']),
        'PICKED'          => floatval($row['QTY_PICKED']),
        'ON_HAND_LIVE'    => floatval($row['QTY_ON_HAND'] - $row['QTY_PICKED']),
        'SALES_ORDERS'    => floatval($row['QTY_ON_SO']),
        'BACK_ORDERS'     => floatval($row['QTY_ON_BO']),
        'FORWARD_ORDERS'  => floatval($row['QTY_ON_FO']),
        'AVAILABLE'       => floatval($row['QTY_AVAILABLE']),
        'PURCHASE_ORDERS' => floatval($row['QTY_ON_PO']),
      );
      $row['DEMAND'] = array(
        'THIS_MONTH'      => floatval($row['THIS_MTH_DEMAND']),
        'MONTHLY_AVERAGE' => floatval($row['AVG_MTH_DEMAND']),
        'DAYS_COVER'      => $days_cover,
        'ACTIVE_MONTHS'   => intval($row['ACTIVE_MONTHS']) % 12,
        'ACTIVE_PERIOD'   => ($row['ACTIVE_MONTHS'] > 12 ? 'ROLLING' : 'PERPETUAL'),
      );
      $row['DATES'] = array(
        'LAST_SALE'       => $row['DATE_LAST_SALE'] == '1899-12-31' ? null : $this->format_date($row['DATE_LAST_SALE']),
        'LAST_STOCKTAKE'  => $row['DATE_LAST_STOCKTAKE'] == '1899-12-31' ? null : $this->format_date($row['DATE_LAST_STOCKTAKE']),
        'LAST_CHANGE'     => $row['DATE_LAST_CHANGE'] == '1899-12-31' ? null : $this->format_date($row['DATE_LAST_CHANGE']),
      );
      unset($row['WHSID'], $row['QTY_ON_HAND'], $row['QTY_AVAILABLE'], $row['QTY_PICKED'],
        $row['QTY_ON_SO'], $row['QTY_ON_BO'], $row['QTY_ON_FO'], $row['QTY_ON_PO'],
        $row['THIS_MTH_DEMAND'], $row['AVG_MTH_DEMAND'], $row['ACTIVE_MONTHS'],
        $row['DATE_LAST_SALE'], $row['DATE_LAST_STOCKTAKE'], $row['DATE_LAST_CHANGE']
      );
      $data[$id] = $row;
    }
    return $data;
  }

}
