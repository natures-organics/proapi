<?php

namespace Products;

class Main extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['prodid'];

    $sql = "
      SELECT
        TRIM(sm.STOCK_CODE)       AS STKID,
        TRIM(sm.STK_DESCRIPTION)  AS STKDESC1,
        TRIM(sm.STK_DESC_LINE_2)  AS STKDESC2,
        TRIM(sm.STK_DESC_LINE_3)  AS STKDESC3,
        TRIM(sm.ONLY_ALPHA4_1)    AS ALPHACODE,
        TRIM(sm.STK_BRAND)        AS BRAND_ID,
        TRIM(b6.sys_description)  AS BRAND_DESC,
        TRIM(sm.STOCK_GROUP)      AS STOCK_GROUP_ID,
        TRIM(pg.sys_description)  AS STOCK_GROUP_DESC,
        TRIM(sm.stk_user_group_2) AS HOLDING_TYPE,
        sm.STK_STOCK_STATUS       AS STOCK_STATUS_ID,
        CASE
          WHEN sm.STK_STOCK_STATUS = 'S' THEN 'Stocked'
          WHEN sm.STK_STOCK_STATUS = 'M' THEN 'Manufactured'
          WHEN sm.STK_STOCK_STATUS = 'L' THEN 'Labour'
          WHEN sm.STK_STOCK_STATUS = 'Z' THEN 'Special'
          ELSE TRIM(sm.STK_STOCK_STATUS)
          END               AS STOCK_STATUS_DESC,
        TRIM(sm.CONDITION_CODE) AS STOCK_CONDITION_ID,
        CASE
          WHEN sm.CONDITION_CODE IN ('', 'A') THEN 'Active'
          WHEN sm.CONDITION_CODE = 'B' THEN 'Rebatable'
          WHEN sm.CONDITION_CODE = 'C' THEN 'Purchase Stop'
          WHEN sm.CONDITION_CODE = 'D' THEN 'Deleted'
          WHEN sm.CONDITION_CODE = 'E' THEN 'Exchange'
          WHEN sm.CONDITION_CODE = 'F' THEN 'Freight/Fee'
          WHEN sm.CONDITION_CODE = 'G' THEN 'Part Shipment Allowed'
          WHEN sm.CONDITION_CODE = 'I' THEN 'Inactive'
          WHEN sm.CONDITION_CODE = 'J' THEN 'Forward Order Only'
          WHEN sm.CONDITION_CODE = 'K' THEN 'Critical'
          WHEN sm.CONDITION_CODE = 'L' THEN 'No Backorders'
          WHEN sm.CONDITION_CODE = 'M' THEN 'Manufactured'
          WHEN sm.CONDITION_CODE = 'N' THEN 'No Supply'
          WHEN sm.CONDITION_CODE = 'O' THEN 'Obsolete'
          WHEN sm.CONDITION_CODE = 'P' THEN 'Packaging'
          WHEN sm.CONDITION_CODE = 'Q' THEN 'QA Hold'
          WHEN sm.CONDITION_CODE = 'R' THEN 'Rotable'
          WHEN sm.CONDITION_CODE = 'S' THEN 'Superseded'
          WHEN sm.CONDITION_CODE = 'T' THEN 'Discontinued'
          WHEN sm.CONDITION_CODE = 'U' THEN 'Used'
          WHEN sm.CONDITION_CODE = 'V' THEN 'Travel'
          WHEN sm.CONDITION_CODE = 'Y' THEN 'Copy Kit'
          WHEN sm.CONDITION_CODE = 'Z' THEN 'Deposit/Receipt'
          ELSE TRIM(sm.CONDITION_CODE)
        END AS STOCK_CONDITION_DESC,
        TRIM(sm.STK_UNIT_DESC)    AS UOM_ID,
        TRIM(um_base.sys_description)  AS UOM_DESC,
        TRIM(sm.stk_pack_desc)    AS PACK_ID,
        TRIM(um_pack.sys_description)  AS PACK_UOM_DESC,
        sm.stk_pack_qty           AS PACK_QTY,
        sm.stk_pack_weight        AS PACK_WEIGHT,
        TRIM(suc_pack.trade_unit_no) AS PACK_TUN,
        suc_pack.suc_weight       AS PACK_WEIGHT,
        suc_pack.suc_length       AS PACK_LENGTH,
        suc_pack.suc_width        AS PACK_WIDTH,
        suc_pack.suc_height       AS PACK_HEIGHT,
        TRIM(sm.STK_APN_NUMBER)   AS TUN,
        sm.SERIALIZED_FLAG        AS SERIALIZED_FLAG_ID,
        CASE
          WHEN sm.SERIALIZED_FLAG = 'N' THEN 'Not Tracked'
          WHEN sm.SERIALIZED_FLAG = 'Y' THEN 'Serial Tracked'
          WHEN sm.SERIALIZED_FLAG = 'L' THEN 'Lot-Tracked'
          WHEN sm.SERIALIZED_FLAG = 'P' THEN 'Purchase Order Tracked'
          ELSE TRIM(sm.SERIALIZED_FLAG)
        END AS SERIALIZED_FLAG_DESC,
        TRIM(sm.REORDER_BUYER)    AS REORDER_BUYER_ID,
        TRIM(rr.sys_description)  AS REORDER_BUYER_DESC,
        TRIM(su.sys_description)  AS SHELF_LIFE,
        sm.CREATION_DATE    AS CREATION_DATE,
        sm.DATE_LAST_CHANGE AS DATE_LAST_CHANGE,
        sm.user_only_num2   AS NOMINAL_VOLUME
      FROM stock_master AS sm
      LEFT JOIN stock_unit_conversion AS suc_pack ON (sm.stock_code = suc_pack.stock_code AND sm.stk_pack_desc = suc_pack.suc_unit_desc)
      LEFT JOIN system_table AS rr ON (rr.sys_tbl_type = 'RR' AND rr.sys_tbl_code = sm.REORDER_BUYER)
      LEFT JOIN system_table AS b6 ON (b6.sys_tbl_type = 'B6' AND b6.sys_tbl_code = sm.STK_BRAND)
      LEFT JOIN system_table AS pg ON (pg.sys_tbl_type = 'PG' AND pg.sys_tbl_code = sm.STOCK_GROUP)
      LEFT JOIN system_table AS su ON (su.sys_tbl_type = 'SU' AND su.sys_tbl_code = sm.SHELF_LIFE_DAYS)
      LEFT JOIN system_table AS um_base ON (um_base.sys_tbl_type = 'UM' AND um_base.sys_tbl_code = sm.STK_UNIT_DESC)
      LEFT JOIN system_table AS um_pack ON (um_pack.sys_tbl_type = 'UM' AND um_pack.sys_tbl_code = sm.STK_PACK_DESC)
      WHERE (UPPER(sm.stock_code) = UPPER(?) OR TRIM(sm.stk_apn_number) = ?)\n";
    $args = array();
    $args[] = $id;
    $args[] = $id;

    $sql .= ' ORDER BY sm.stock_code';
    $res = $odbc->query($sql, $args);
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  private function massage_arrays($res) {
    foreach ($res as $row) {
      $id = $row['STKID'];
      $row['NOMINAL_VOLUME'] = floatval($row['NOMINAL_VOLUME']);
      $row['DESCRIPTION'] = array(
        'LINE1' => $row['STKDESC1'],
        'LINE2' => $row['STKDESC2'],
        'LINE3' => $row['STKDESC3'],
      );
      $row['STATUS'] = array(
        'ID'          => $row['STOCK_STATUS_ID'],
        'DESCRIPTION' => $row['STOCK_STATUS_DESC'],
      );
      unset($row['STOCK_STATUS_ID'], $row['STOCK_STATUS_DESC']);

      $row['CONDITION'] = array(
        'ID'          => $row['STOCK_CONDITION_ID'],
        'DESCRIPTION' => $row['STOCK_CONDITION_DESC'],
      );
      unset($row['STOCK_CONDITION_ID'], $row['STOCK_CONDITION_DESC']);

      $row['SERIALIZED'] = array(
        'ID'          => $row['SERIALIZED_FLAG_ID'],
        'DESCRIPTION' => $row['SERIALIZED_FLAG_DESC'],
      );
      unset($row['SERIALIZED_FLAG_ID'], $row['SERIALIZED_FLAG_DESC']);

      $row['GROUP'] = array(
        'ID'          => $row['STOCK_GROUP_ID'],
        'DESCRIPTION' => $row['STOCK_GROUP_DESC'],
      );
      $row['BRAND'] = array(
        'ID'          => $row['BRAND_ID'],
        'DESCRIPTION' => $row['BRAND_DESC'],
      );
      $row['UOM'] = array(
        'BASE' => array(
          'ID'          => $row['UOM_ID'],
          'DESCRIPTION' => $row['UOM_DESC'],
          'TUN'         => $row['TUN'],
        ),
        'PACK' => array(
          'ID'        => $row['PACK_ID'],
          'DESCRIPTION' => $row['PACK_UOM_DESC'],
          'QUANTITY'  => floatval($row['PACK_QTY']),
          'WEIGHT'    => floatval($row['PACK_WEIGHT']),
          'LENGTH'    => floatval($row['PACK_LENGTH']),
          'WIDTH'     => floatval($row['PACK_WIDTH']),
          'HEIGHT'    => floatval($row['PACK_HEIGHT']),
          'CUBIC'     => $row['PACK_LENGTH'] * $row['PACK_WIDTH'] * $row['PACK_HEIGHT'],
          'TUN'       => $row['PACK_TUN'],
        ),
      );
      $row['REORDER_BUYER'] = array(
        'ID'          => $row['REORDER_BUYER_ID'],
        'DESCRIPTION' => $row['REORDER_BUYER_DESC'],
      );
      $row['DATES'] = array(
        'CREATED' => $this->format_date($row['CREATION_DATE']),
        'UPDATED' => $this->format_date($row['DATE_LAST_CHANGE']),
      );
      unset($row['STKID'], $row['TUN'], $row['PACK_UOM_DESC'],
        $row['PACK_ID'], $row['PACK_QTY'], $row['PACK_WEIGHT'], $row['PACK_TUN'],
        $row['PACK_LENGTH'], $row['PACK_WIDTH'], $row['PACK_HEIGHT'],
        $row['STKDESC1'], $row['STKDESC2'], $row['STKDESC3'],
        $row['STOCK_GROUP_ID'], $row['STOCK_GROUP_DESC'],
        $row['BRAND_ID'], $row['BRAND_DESC'],
        $row['UOM_ID'], $row['UOM_DESC'],
        $row['REORDER_BUYER_ID'], $row['REORDER_BUYER_DESC'],
        $row['CREATION_DATE'], $row['DATE_LAST_CHANGE']
      );
      $results[$id] = $row;
    }
    return $results;
  }
}
