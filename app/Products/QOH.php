<?php

namespace Products;

class QOH extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['prodid'];
    $whs = $params['whs'] ?: null;

    $sql = "SELECT
        swd.whse_code           AS WHSID,
        TRIM(sm.stk_unit_desc)  AS UOM,
        swd.whse_qty_on_hand    AS QTY_ON_HAND,
        swd.qty_on_order        AS QTY_ON_PO,
        swd.current_orders      AS QTY_ON_SO,
        swd.whse_back_orders    AS QTY_ON_BO,
        swd.forward_orders      AS QTY_ON_FO,
        (SELECT
          SUM(CASE
            WHEN solpl.solpl_packed_qty IS NULL THEN solp.solpk_qty
            ELSE solpl.solpl_packed_qty
          END) AS packed_qty
          FROM sales_order_line AS sol
          JOIN sales_order AS so
            ON (sol.so_order_no = so.so_order_no
            AND sol.so_bo_suffix = so.so_bo_suffix)
          JOIN sales_order_line_packages AS solp
            ON (solp.so_bo_suffix = sol.so_bo_suffix
            AND sol.so_order_no = solp.so_order_no
            AND sol.sol_line_seq = solp.sol_line_seq)
          LEFT OUTER JOIN sales_order_line_pkg_lots AS solpl
            ON (solp.sopk_sscc = solpl.sopk_sscc)
          WHERE so.so_whse_code = swd.whse_code
            AND so.so_order_status IN ('40', '80')
            AND sol.stock_code = swd.stock_code
            AND solp.solpk_qty IS NOT NULL
          ) AS QTY_PICKED,
        swd.whse_qty_on_hand - swd.current_orders AS QTY_AVAILABLE,
        swd.monthly_demand      AS AVG_MTH_DEMAND,
        swd.this_mth_demand     AS THIS_MTH_DEMAND,
        swd.whse_active_mths    AS ACTIVE_MONTHS,
        TRIM(swd.bin_location)  AS BIN_LOCATION,
        swd.stock_last_sale     AS DATE_LAST_SALE,
        swd.stock_take_date     AS DATE_LAST_STOCKTAKE,
        swd.date_last_change    AS DATE_LAST_CHANGE
      FROM stock_warehouse_detail AS swd
      JOIN stock_master AS sm ON (swd.stock_code = sm.stock_code)
      WHERE UPPER(swd.stock_code) = UPPER(?)";
    $args[] = $id;
    if ( $whs ) {
      $sql .= ' AND (UPPER(TRIM(swd.whse_code)) = UPPER(?))';
      $args[] = $whs;
    }
    $sql .= ' ORDER BY swd.whse_code';
    $res = $odbc->query($sql, $args);

    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }
  
  private function massage_arrays($res) {

    $data = array();
    foreach ($res as $row) {
      $id = $row['WHSID'];
      if (floatval($row['AVG_MTH_DEMAND']) > 0) {
        $days_cover = round(((floatval($row['QTY_ON_HAND']) / floatval($row['AVG_MTH_DEMAND'])) * 30), 1);
      } else {
        $days_cover = null;
      }
      $row['QUANTITY'] = array(
        'ON_HAND'         => floatval($row['QTY_ON_HAND']),
        'PICKED'          => floatval($row['QTY_PICKED']),
        'ON_HAND_LIVE'    => floatval($row['QTY_ON_HAND'] - $row['QTY_PICKED']),
        'SALES_ORDERS'    => floatval($row['QTY_ON_SO']),
        'BACK_ORDERS'     => floatval($row['QTY_ON_BO']),
        'FORWARD_ORDERS'  => floatval($row['QTY_ON_FO']),
        'AVAILABLE'       => floatval($row['QTY_AVAILABLE']),
        'PURCHASE_ORDERS' => floatval($row['QTY_ON_PO']),
      );
      $row['DEMAND'] = array(
        'THIS_MONTH'      => floatval($row['THIS_MTH_DEMAND']),
        'MONTHLY_AVERAGE' => floatval($row['AVG_MTH_DEMAND']),
        'DAYS_COVER'      => $days_cover,
        'ACTIVE_MONTHS'   => intval($row['ACTIVE_MONTHS']) % 12,
        'ACTIVE_PERIOD'   => ($row['ACTIVE_MONTHS'] > 12 ? 'ROLLING' : 'PERPETUAL'),
      );
      $row['DATES'] = array(
        'LAST_SALE'       => $row['DATE_LAST_SALE'] == '1899-12-31' ? null : $this->format_date($row['DATE_LAST_SALE']),
        'LAST_STOCKTAKE'  => $row['DATE_LAST_STOCKTAKE'] == '1899-12-31' ? null : $this->format_date($row['DATE_LAST_STOCKTAKE']),
        'LAST_CHANGE'     => $row['DATE_LAST_CHANGE'] == '1899-12-31' ? null : $this->format_date($row['DATE_LAST_CHANGE']),
      );
      unset($row['WHSID'], $row['QTY_ON_HAND'], $row['QTY_AVAILABLE'], $row['QTY_PICKED'],
        $row['QTY_ON_SO'], $row['QTY_ON_BO'], $row['QTY_ON_FO'], $row['QTY_ON_PO'],
        $row['THIS_MTH_DEMAND'], $row['AVG_MTH_DEMAND'], $row['ACTIVE_MONTHS'],
        $row['DATE_LAST_SALE'], $row['DATE_LAST_STOCKTAKE'], $row['DATE_LAST_CHANGE']
      );
      $data[$id] = $row;
    }
    return $data;
  }

}
