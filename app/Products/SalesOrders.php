<?php

namespace Products;

class SalesOrders extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['prodid'];
    $where_status   = $f3->get('REQUEST.status') ?: null;
    $where_customer = $f3->get('REQUEST.customer') ?: null;

    $sql = "SELECT
        so.so_order_no              AS SO_NUM,
        TRIM(so.so_bo_suffix)       AS SO_SUF,
        sol.sol_line_seq            AS LINE_SEQ,
        so.so_order_status          AS STATUS,
        TRIM(so.so_cust_code)       AS CUSTID,
        (sol.sol_ordered_qty * suc.unit_conversion)::INTEGER AS QTY_ORDERED,
        (sol.sol_shipped_qty * suc.unit_conversion)::INTEGER AS QTY_SHIPPED,
        TRIM(sq.sys_tbl_code)       AS SHORT_SHIP_CODE,
        TRIM(sq.sys_description)    AS SHORT_SHIP_REASON
      FROM sales_order_line_all AS sol
      JOIN sales_order_all AS so ON (so.so_order_no = sol.so_order_no AND so.so_bo_suffix = sol.so_bo_suffix)
      JOIN stock_unit_conversion AS suc ON (sol.stock_code = suc.stock_code AND sol.stk_unit_desc = suc.suc_unit_desc)
      LEFT JOIN system_table AS sq ON (sq.sys_tbl_type = 'SQ' AND sol.ship_reason_code = sq.sys_tbl_code)
      WHERE (UPPER(TRIM(sol.stock_code)) = UPPER(?))";
    $args[] = $id;
    if ( $where_status ) {
      $sql .= ' AND (so.so_order_status = ?)';
      $args[] = $where_status;
    }
    if ( $where_customer ) {
      $sql .= ' AND (UPPER(TRIM(so.so_cust_code)) = UPPER(?))';
      $args[] = $where_customer;
    }

    $sql .= ' ORDER BY so.so_order_no, so.so_bo_suffix, sol.sol_line_seq';
    $res = $odbc->query($sql, $args);
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
    
  }
  
  private function massage_arrays($res) {
    $results = array();
    foreach ($res as $row) {
      $row['ORDER_NUM'] = $row['SO_NUM'].$row['SO_SUF'];
      $row['LINE_SEQ'] = floatval($row['LINE_SEQ']);
      $row['QUANTITY'] = array(
        'ORDERED' => floatval($row['QTY_ORDERED']),
        'SHIPPED' => floatval($row['QTY_SHIPPED']),
      );
      if ($row['SHORT_SHIP_CODE'] ) 
        $row['SHORT_SHIP'] = array(
          'CODE' => $row['SHORT_SHIP_CODE'],
          'REASON' => $row['SHORT_SHIP_REASON'],
        );

      unset($row['SO_NUM'], $row['SO_SUF'],
        $row['QTY_ORDERED'], $row['QTY_SHIPPED'],
        $row['SHORT_SHIP_CODE'], $row['SHORT_SHIP_REASON']
      );
      $results[] = $row;
    }
    return $results;
  }

}
