<?php

namespace Products;

class UOM extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['prodid'];
    $uom = $params['uom'] ?: null;

    $sql = "SELECT  
      TRIM(suc.suc_unit_desc) AS SUC_UNIT_DESC,
      suc.unit_conversion AS CONVERSION_FACTOR,
      TRIM(suc.trade_unit_no) AS TRADE_UNIT_NO,
      suc.suc_weight  AS WEIGHT,
      suc.suc_length  AS LENGTH,
      suc.suc_width   AS WIDTH,
      suc.suc_height  AS HEIGHT,
      TRIM(suc.suc_tun_status) AS TUN_STATUS,
      TRIM(suc.suc_own_box_code) AS OWN_BOX_CODE,
      TRIM(suc.publish_flag) AS PUBLISH_FLAG,
      sm.stk_unit_desc = suc.suc_unit_desc PRIMARY_UOM,
      sm.stk_pack_desc = suc.suc_unit_desc PACK_UOM
      FROM stock_unit_conversion AS suc
      JOIN stock_master AS sm ON (sm.stock_code = suc.stock_code)
      WHERE UPPER(suc.stock_code) = UPPER(?)";
    $args[] = $id;
    if ( $uom ) {
      $sql .= ' AND (UPPER(TRIM(suc.suc_unit_desc)) = UPPER(?))';
      $args[] = $uom;
    }
    $sql .= ' ORDER BY suc.suc_unit_desc';
    $res = $odbc->query($sql, $args);

    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }
  
  private function massage_arrays($res) {
    $primary_uom = null;
    $pack_uom = null;
    foreach ($res as $row) {
      $uom = $row['SUC_UNIT_DESC'];
      $row['CONVERSION_FACTOR'] = floatval($row['CONVERSION_FACTOR']);
      $row['DIMENSIONS'] = array(
        'LENGTH'  => floatval($row['LENGTH']),
        'WIDTH'   => floatval($row['WIDTH']),
        'HEIGHT'  => floatval($row['HEIGHT']),
        'CUBIC'   => $row['LENGTH'] * $row['WIDTH'] * $row['HEIGHT'],
        'WEIGHT'  => floatval($row['WEIGHT']),
      );
      $row['PRIMARY_UOM'] = $this->convert_string_to_boolean($row['PRIMARY_UOM']);
      $row['PACK_UOM'] = $this->convert_string_to_boolean($row['PACK_UOM']);
      if ($row['PRIMARY_UOM']) $primary_uom = $uom;
      if ($row['PACK_UOM']) $pack_uom = $uom;
      unset($row['STOCK_CODE'], $row['SUC_UNIT_DESC'],
            $row['WEIGHT'], $row['LENGTH'], $row['WIDTH'], $row['HEIGHT']
      );
      $results[$uom] = $row;
    }
    $results['PRIMARY_UOM'] = $primary_uom;
    $results['PACK_UOM'] = $pack_uom;
    return $results;
  }

}
