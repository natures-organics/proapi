<?php

namespace PurchaseOrders;

class Index extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $where_supplier   = $f3->get('REQUEST.supplier') ?: null;
    $where_status     = $f3->get('REQUEST.status') ?: null;

    $sql = "
      SELECT
      CONCAT(po.po_order_no, TRIM(backorder_flag)) AS ID,
        TRIM(po.cre_accountcode)  AS CREDITOR_ID,
        po.po_order_status        AS STATUS
      FROM purchase_order_all AS po
      LEFT JOIN cre_master AS cre ON (po.cre_accountcode = cre.cre_accountcode)
      WHERE 1=1";
    $args = array();
    if ( $where_supplier ) {
      $sql .= ' AND (UPPER(cre.cre_accountcode) = UPPER(?))';
      $args[] = $where_supplier;
    }
    if ( $where_status ) {
      $sql .= ' AND (po.po_order_status = ?)';
      $args[] = $where_status;
    }
    if (!$args) $f3->error(400); // no filtering given; refuse to return entire table
    $sql .= ' ORDER BY po.po_order_no ASC, po.backorder_flag ASC';
    $this->return_data2client($odbc->query($sql, $args));
  }

}
