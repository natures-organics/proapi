<?php

namespace PurchaseOrders;

class Lines extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['poid'];

    // split the order id into 'number' and 'suffix' parts because they are
    // different columns in the database.
    $order['num'] = preg_replace('/[^\d*]/i', '', $id);
    $order['suf'] = preg_replace('/[^A-Z*]/i', '', $id);

    $sql = "
      SELECT
        pol.po_l_seq                AS LINE_SEQ,
        pol.po_line_type            AS LINE_TYPE_ID,
        poltd.description           AS LINE_TYPE_DESCRIPTION,
        pol.po_line_type = 'SN'     AS IS_STOCK_ITEM,
        TRIM(pol.stock_code)        AS STOCK_CODE,
        TRIM(pol.supp_stk_code)     AS SUPPLIER_STOCK_CODE,
        CASE WHEN pol.po_line_type = 'SN' THEN sm.stk_description ELSE TRIM(pol.line_description) END AS LINE_DESCRIPTION,
        pol.unit_description        AS UOM,
        pol.unit_conversion,
        pol.po_order_qty            AS QTY_ORDERED,
        pol.po_received_qty         AS QTY_RECEIVED,
        pol.po_backorder_qty        AS QTY_BACKORDERED,
        pol.date_expected
      FROM purchase_order_line AS pol
      JOIN purchase_order_line_type_desc AS poltd ON (pol.po_line_type = poltd.poltd_line_type AND poltd.poltd_language = '')
      LEFT JOIN stock_master AS sm ON (sm.stock_code = pol.stock_code)
      WHERE (pol.po_order_no = ? AND pol.backorder_flag = ?)
      ORDER BY pol.po_l_seq";
    $args = array();
    $args[] = $order['num'];
    $args[] = $order['suf'];

    $res = $odbc->query($sql, $args);
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  private function massage_arrays($lines) {
    $f3=\Base::instance();

    foreach ($lines as $pol) {
      $pol_seq = floatval($pol['LINE_SEQ']);
      $pol['LINE_TYPE'] = array(
        'ID'          => $pol['LINE_TYPE_ID'],
        'DESCRIPTION' => $pol['LINE_TYPE_DESCRIPTION'],
      );

      if ($pol['LINE_TYPE_ID'] != 'DM' and $pol['LINE_TYPE_ID'] != 'DN') {
        $pol['QUANTITIES'] = array(
          'ORDERED'     => floatval($pol['QTY_ORDERED']),
          'RECEIVED'    => floatval($pol['QTY_RECEIVED']),
          'BACKORDERED' => floatval($pol['QTY_BACKORDERED']),
          'UOM'         => $pol['UOM'],
          'CONVERSION'  => floatval($pol['UNIT_CONVERSION']),
        );
      }

      $pol['IS_STOCK_ITEM'] = $this->convert_string_to_boolean($pol['IS_STOCK_ITEM']);
      unset($pol['LINE_SEQ'], $pol['LINE_TYPE_ID'], $pol['LINE_TYPE_DESCRIPTION'],
        $pol['QTY_ORDERED'], $pol['QTY_RECEIVED'], $pol['QTY_BACKORDERED'],
        $pol['UOM'], $pol['UNIT_CONVERSION']
      );
      $results[$pol_seq] = $pol;
    }

    return $results;
  }
  
}
