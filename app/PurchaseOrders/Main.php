<?php

namespace PurchaseOrders;

class Main extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['poid'];

    // split the order id into 'number' and 'suffix' parts because they are
    // different columns in the database.
    $order['num'] = preg_replace('/[^\d*]/i', '', $id);
    $order['suf'] = preg_replace('/[^A-Z*]/i', '', $id);

    $sql = "
      SELECT
        po.po_order_no            AS PO_ID,
        TRIM(po.backorder_flag)   AS BACKORDER_FLAG,
        TRIM(po.cre_accountcode)  AS SUPPLIER_CODE,
        TRIM(cre.cr_shortname)    AS SUPPLIER_NAME,
        po.po_order_date,
        po.po_arrival_date,
        po.po_received_date,
        po.po_order_status        AS ORDER_STATUS_CODE,
        posd.posd_description     AS ORDER_STATUS_DESC,
        TRIM(po.po_inwards_no)    AS INWARDS_NO,
        po.po_whse_code,
        TRIM(po.invoice_details)  AS INVOICE_NO,
        TRIM(po.po_vessel_name)   AS VESSEL_NAME,
        TRIM(po.po_entry_port)    AS ENTRY_PORT,
        TRIM(po.po_voyage_no)     AS VOYAGE_NO,
        TRIM(po.po_lc_number)     AS LC_NUMBER,
        TRIM(po.po_shipment_no)   AS SHIPMENT_NO,
        TRIM(po.po_user_name)     AS KEYED_BY,
        -- carrier_code == the code for the delivery instruction table, but that
        -- info is retrieved from the delivery instructions of the po
        --TRIM(po.po_carrier_code)  AS CARRIER_CODE,
        po.po_revision_no,
        po.po_order_type,
        TRIM(po.receiver_user_id) AS RECEIVED_BY,
        po.pay_by_date,
        po.invoice_due_date
      FROM purchase_order_all AS po
      JOIN purchase_order_status_desc AS posd ON (po.po_order_status = posd.posd_status AND posd.posd_language = '')
      LEFT JOIN cre_master AS cre ON (po.cre_accountcode = cre.cre_accountcode)
      WHERE (po.po_order_no = ? AND po.backorder_flag = ?)";
    $args = array();
    $args[] = $order['num'];
    $args[] = $order['suf'];

    $res = $odbc->query($sql, $args);
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  private function massage_arrays($headers) {
    $f3=\Base::instance();

    foreach ($headers as $po) {
      $po_id = $po['PO_ID'].$po['BACKORDER_FLAG'];
      $po['SUPPLIER'] = array(
        'ID'    => $po['SUPPLIER_CODE'],
        'NAME'  => $po['SUPPLIER_NAME'],
      );
      $po['STATUS'] = array(
        'ID'          => $po['ORDER_STATUS_CODE'],
        'DESCRIPTION' => $po['ORDER_STATUS_DESC'],
      );
      $po['DATES'] = array(
        'ORDER'   => $po['PO_ORDER_DATE'] == '1899-12-31' ? null : $this->format_date($po['PO_ORDER_DATE']),
        'ARRIVAL' => $po['PO_ARRIVAL_DATE'] == '1899-12-31' ? null : $this->format_date($po['PO_ARRIVAL_DATE']),
        'RECEIPT' => $po['PO_RECEIVED_DATE'] == '1899-12-31' ? null : $this->format_date($po['PO_RECEIVED_DATE']),
      );
      $po['DELIVERY_INSTRUCTIONS'] = $this->fetch_delivery_instructions($po_id);
      // strip all the keys we've restructured above
      unset(
        $po['PO_ID'], $po['BACKORDER_FLAG'],
        $po['SUPPLIER_CODE'], $po['SUPPLIER_NAME'],
        $po['ORDER_STATUS_CODE'], $po['ORDER_STATUS_DESC'],
        $po['PO_ORDER_DATE'], $po['PO_ARRIVAL_DATE'], $po['PO_RECEIVED_DATE']
      );
      $results[$po_id] = $po;
    }

    return $results;
  }
  
  private function fetch_delivery_instructions($id) {
    if (!$id) return;
    
    $f3=\Base::instance();
    $odbc = \ODBC::instance();

    // split the order id into 'number' and 'suffix' parts because they are
    // different columns in the database.
    $order['num'] = preg_replace('/[^\d*]/i', '', $id);
    $order['suf'] = preg_replace('/[^A-Z*]/i', '', $id);

    $sql = "SELECT
        TRIM(pod.po_dl_text_1)  AS DEL1,
        TRIM(pod.po_dl_text_2)  AS DEL2,
        TRIM(pod.po_dl_text_3)  AS DEL3,
        TRIM(pod.po_dl_text_4)  AS DEL4,
        TRIM(pod.po_dl_text_5)  AS DEL5,
        TRIM(pod.po_dl_text_6)  AS DEL6,
        TRIM(pod.po_dl_text_7)  AS DEL7,
        TRIM(pod.po_dl_postcode)  AS DELPC,
        TRIM(pod.dl_country_code) AS DELCC,
        TRIM(pod.po_dl_phone)     AS DELPHONE,
        TRIM(pod.po_dl_fax)       AS DELFAX,
        TRIM(pod.mobile_phone)    AS DELMOBILE,
        TRIM(pod.po_dl_email)     AS DELEMAIL
      FROM pronto.purchase_order_delivery AS pod
      WHERE (pod.po_order_no = ? AND pod.backorder_flag = ?)
        AND (pod.po_dl_type = 'DI') -- delivery instruction";
    $res = $odbc->query($sql, array($order['num'], $order['suf']));
    if ($res) return array_filter(array_values($res[0]));
    return null;
  }
}
