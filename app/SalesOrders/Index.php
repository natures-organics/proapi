<?php

namespace SalesOrders;

class Index extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $where_cust       = $f3->get('REQUEST.customer') ?: null;
    $where_cust_ref   = $f3->get('REQUEST.custref') ?: null;
    $where_territory  = $f3->get('REQUEST.territory') ?: null;
    $where_status     = $f3->get('REQUEST.status') ?: null;

    $sql = "SELECT
        CONCAT(so.so_order_no, TRIM(so.so_bo_suffix)) AS ID,
        TRIM(dm_child.accountcode)  AS CUSTOMER_ID,
        TRIM(so.cust_reference)     AS CUSTOMER_REFERENCE,
        so.so_order_status AS STATUS
      FROM sales_order_all AS so
      JOIN deb_master AS dm_child  ON (so.so_cust_code = dm_child.accountcode)
      JOIN deb_master AS dm_parent ON (dm_child.bill_to = dm_parent.accountcode)
      WHERE 1=1\n";
    $args = array();
    $sort_order = 'so.so_order_no, so.so_bo_suffix';
    if ( $where_cust ) {
      $sql .= ' AND (UPPER(dm_child.accountcode) = UPPER(?))';
      $args[] = $where_cust;
    }
    if ( $where_cust_ref ) {
      $sql .= ' AND (UPPER(TRIM(so.cust_reference)) = UPPER(?))';
      $args[] = $where_cust_ref;
    }
    if ( $where_territory ) {
      $sql .= ' AND (UPPER(dm_child.territory) = UPPER(?))';
      $args[] = $where_territory;
    }
    if ( $where_status ) {
      // allow multiple statuses to be retrieved (comma-separated)
      // eg: status=30  or  status=20,30
      if (!preg_match('/^([0-9]+,?)+$/', $where_status)) $f3->error(422);
      $statuses = explode(',', $where_status);
      $in = join(',', array_fill(0, count($statuses), '?'));
      $sql .= " AND (so.so_order_status IN ($in))";
      $args = array_merge($args, $statuses);
      unset($in, $statuses);
      $sort_order = 'so.so_order_status, '.$sort_order;
    }
    $sql .= "\nORDER BY $sort_order";
    if (!$args) $f3->error(400); // no filtering given; refuse to return entire table

    $res = $odbc->query($sql, $args);
    $this->return_data2client($res);
  }

}
