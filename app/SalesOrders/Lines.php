<?php

namespace SalesOrders;

class Lines extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $res_lines = null;
    $id = $params['soid'];
    
    if ($id && !$this->is_valid_so_number($id))
      $f3->error(422);

    // split the order id into 'number' and 'suffix' parts because they are
    // different columns in the database.
    $order['num'] = preg_replace('/[^\d*]/i', '', $id);
    $order['suf'] = preg_replace('/[^A-Z*]/i', '', $id);

    $sql = "
      SELECT
        sol.sol_line_seq            AS LINE_SEQ,
        sol.sol_line_type           AS LINE_TYPE_ID,
        soltd.description           AS LINE_TYPE_DESCRIPTION,
        sol.sol_line_type = 'SN' AS IS_STOCK_ITEM,
        CASE WHEN sol.sol_line_type = 'SN' THEN TRIM(sm.stock_code) ELSE NULL END AS LINE_ITEM,
        CASE WHEN sol.sol_line_type = 'SN' THEN TRIM(sm.stk_description) ELSE TRIM(sol.line_description) END AS LINE_DESCRIPTION,
        TRIM(sm.stk_unit_desc)      AS UOM,
        -- normalize to base UOM
        CAST((sol.sol_ordered_qty * suc.unit_conversion) AS INTEGER) AS QTY_ORDERED,
        CAST((sol.sol_shipped_qty * suc.unit_conversion) AS INTEGER) AS QTY_SHIPPED,
        suc_plt.unit_conversion AS PLT_QTY,
        suc_lyr.unit_conversion AS LYR_QTY,
        CAST((NVL(so_pick.sol_picked_qty, 0) * suc.unit_conversion) AS INTEGER) AS QTY_PICKED,
        TRIM(so_pick.pick_reference)      AS PICK_REFERENCE,  --batch number
        TRIM(sq.sys_tbl_code) AS SHORT_SHIP_CODE, -- use sys table to get a null instead of blank string when no code
        TRIM(sq.sys_description)    AS SHORT_SHIP_REASON
      FROM sales_order_line_all AS sol
      LEFT JOIN stock_master AS sm ON (sol.stock_code = sm.stock_code)
      -- left join suc because special/note/memo lines don't have a UOM
      LEFT JOIN stock_unit_conversion AS suc
        ON (sm.stock_code = suc.stock_code AND sol.stk_unit_desc = suc.suc_unit_desc)
      LEFT JOIN stock_unit_conversion AS suc_lyr
        ON (sm.stock_code = suc_lyr.stock_code AND suc_lyr.suc_unit_desc = 'LYR')
      LEFT JOIN stock_unit_conversion AS suc_plt
        ON (sm.stock_code = suc_plt.stock_code AND suc_plt.suc_unit_desc = 'PLT')
      LEFT JOIN system_table AS sq ON (sq.sys_tbl_type = 'SQ' AND sol.ship_reason_code = sq.sys_tbl_code)
      LEFT JOIN 
        (SELECT so_order_no, so_bo_suffix, sol_line_seq, sol_picked_qty, pick_reference FROM sales_order_picking_loc_all
          UNION
         SELECT so_order_no, so_bo_suffix, sol_line_seq, sol_picked_qty, pick_reference FROM  sales_order_picking_loc_arch
        ) AS so_pick
        ON (sol.so_order_no = so_pick.so_order_no
        AND sol.so_bo_suffix = so_pick.so_bo_suffix
        AND so_pick.sol_line_seq = sol.sol_line_seq)
      JOIN sales_order_line_type_desc AS soltd ON (sol.sol_line_type = soltd.soltd_type AND soltd.soltd_language = '')
      WHERE (sol.so_order_no = ? AND sol.so_bo_suffix = ?)
      ORDER BY sol.sol_line_seq";
    $res_lines = $odbc->query($sql, array($order['num'], $order['suf']));

    $data = $this->massage_arrays($res_lines);
    $this->return_data2client($data);
  }

  private function massage_arrays($lines) {
    foreach ($lines as $sol) {
      $sol_seq = floatval($sol['LINE_SEQ']);
      $line_type = $sol['LINE_TYPE_ID'];
      $sol['LINE_TYPE'] = array(
        'ID'          => $sol['LINE_TYPE_ID'],
        'DESCRIPTION' => $sol['LINE_TYPE_DESCRIPTION'],
      );
      // skip quantities etc on note lines
      if ($line_type != 'DN') {
        $sol['QUANTITY'] = array(
          'ORDERED' => floatval($sol['QTY_ORDERED']),
          'SHIPPED' => floatval($sol['QTY_SHIPPED']),
          'PICKED'  => floatval($sol['QTY_PICKED']),
          'UOM'     => $sol['UOM'],
        );
        if ($sol['PLT_QTY'] > 0) {
          $ctn_cnt = $sol['QTY_SHIPPED'];
          $plt_qty = intval($sol['PLT_QTY']);
          $lyr_qty = intval($sol['LYR_QTY']);
          $plt_cnt = floor($ctn_cnt / $plt_qty);
          $ctn_cnt = $ctn_cnt % $plt_qty;
          $lyr_cnt = floor($ctn_cnt / $lyr_qty);
          $ctn_cnt = $ctn_cnt % $lyr_qty;
          $sol['SHIPPING_UNITS'] = array(
            'PALLETS' => $plt_cnt,
            'LAYERS'  => $lyr_cnt,
            'CARTONS' => $ctn_cnt,
          );
        }
        if ($sol['SHORT_SHIP'])
          $sol['SHORT_SHIP'] = array(
            'ID'          => $sol['SHORT_SHIP_CODE'],
            'DESCRIPTION' => $sol['SHORT_SHIP_REASON'],
          );
      }
      $sol['IS_STOCK_ITEM'] = $this->convert_string_to_boolean($sol['IS_STOCK_ITEM']);
      unset(
        $sol['LINE_SEQ'], $sol['LINE_TYPE_ID'], $sol['LINE_TYPE_DESCRIPTION'],
        $sol['QTY_ORDERED'], $sol['QTY_SHIPPED'], $sol['QTY_PICKED'], $sol['UOM'],
        $sol['SHORT_SHIP_CODE'], $sol['SHORT_SHIP_REASON'],
        $sol['PLT_QTY'], $sol['LYR_QTY']
      );
      $results[$sol_seq] = $sol;
    }

    return $results;
  }

}
