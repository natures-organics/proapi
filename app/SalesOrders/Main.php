<?php

namespace SalesOrders;

class Main extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['soid'];
    if (!$this->is_valid_so_number($id))
      $f3->error(422);

    // split the order id into 'number' and 'suffix' parts because they are
    // different columns in the database.
    $order['num'] = preg_replace('/[^\d*]/i', '', $id);
    $order['suf'] = preg_replace('/[^A-Z*]/i', '', $id);

    $sql = "SELECT
        so.so_order_no                AS ORDER_NUM,
        TRIM(so.so_bo_suffix)         AS ORDER_SUF,
        TRIM(dm_child.accountcode)    AS CUST_CHILD_CODE,
        TRIM(dm_child.shortname)      AS CUST_CHILD_NAME,
        TRIM(dm_parent.accountcode)   AS CUST_PARENT_CODE,
        TRIM(dm_parent.shortname)     AS CUST_PARENT_NAME,
        so.so_order_date              AS ORDER_DATE,
        so.so_delivery_date           AS DEL_DATE,
        so.so_order_status            AS ORDER_STATUS_CODE,
        UPPER(sosd.sosd_description)  AS ORDER_STATUS_DESC,
        so.so_whse_code               AS WAREHOUSE,
        TRIM(so.cust_reference)       AS CUST_REFERENCE,
        TRIM(so.consignment_note)     AS CONSIGNMENT_NUMBER,
        TRIM(so.so_user_id_code)      AS KEYED_BY
      FROM sales_order_all AS so
      LEFT JOIN deb_master AS dm_child  ON (so.so_cust_code = dm_child.accountcode)
      LEFT JOIN deb_master AS dm_parent ON (dm_child.bill_to = dm_parent.accountcode)
      LEFT JOIN system_table AS hr ON (hr.sys_tbl_type = 'HR' AND so.reason_code = hr.sys_tbl_code)
      JOIN sales_order_status_desc AS sosd ON (so.so_order_status = sosd.sosd_status AND sosd.sosd_language = '')
      WHERE (so.so_order_no = ? AND so.so_bo_suffix = ?)";
    $args = array();
    $args[] = $order['num'];
    $args[] = $order['suf'];

    $sql .= ' ORDER BY so.so_order_no, so.so_bo_suffix';
    $res = $odbc->query($sql, $args);
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  private function massage_arrays($headers) {
    $f3=\Base::instance();

    foreach ($headers as $so) {
      $so_id = $so['ORDER_NUM'].$so['ORDER_SUF'];
      $so['CUSTOMER'] = array(
        'CHILD'   => array('ID' => $so['CUST_CHILD_CODE'], 'NAME' => $so['CUST_CHILD_NAME']),
        'PARENT'  => array('ID' => $so['CUST_PARENT_CODE'], 'NAME' => $so['CUST_PARENT_NAME'])
      );
      $so['ORDER_STATUS'] = array(
        'ID'          => $so['ORDER_STATUS_CODE'],
        'DESCRIPTION' => $so['ORDER_STATUS_DESC'],
      );
      $so['DATES'] = array(
        'ORDERED'   => $this->format_date($so['ORDER_DATE']),
        'DELIVERY'  => $this->format_date($so['DEL_DATE']),
      );
      $so['DELIVERY_ADDRESS'] = $this->fetch_delivery_address($so_id);
      $so['DELIVERY_INSTRUCTIONS'] = $this->fetch_delivery_instructions($so_id);
      // strip all the keys we've restructured above
      unset(
        $so['ORDER_NUM'], $so['ORDER_SUF'],
        $so['ORDER_DATE'], $so['DEL_DATE'],
        $so['ORDER_STATUS_CODE'], $so['ORDER_STATUS_DESC'],
        $so['CUST_CHILD_CODE'], $so['CUST_CHILD_NAME'],
        $so['CUST_PARENT_CODE'], $so['CUST_PARENT_NAME']
      );
      $results[$so_id] = $so;
    }

    return $results;
  }
  
  private function fetch_delivery_address($id) {
    return $this->fetch_soda($id, 'DA');
  }
  private function fetch_delivery_instructions($id) {
    return $this->fetch_soda($id, 'DI');
  }

  /*
   * soda = sales_order_delivery_all
   */
  private function fetch_soda($id, $da_type) {
    if (!id) return;
    $f3=\Base::instance();
    $odbc = \ODBC::instance();
    // split the order id into 'number' and 'suffix' parts because they are
    // different columns in the database.
    $order['num'] = preg_replace('/[^\d*]/i', '', $id);
    $order['suf'] = preg_replace('/[^A-Z*]/i', '', $id);

    $sql = "SELECT
        TRIM(soda.so_dl_text_1)      AS LINE1,
        TRIM(soda.so_dl_text_2)      AS LINE2,
        TRIM(soda.so_dl_text_3)      AS LINE3,
        TRIM(soda.so_dl_text_4)      AS LINE4,
        TRIM(soda.so_dl_text_5)      AS LINE5,
        TRIM(soda.so_dl_text_6)      AS LINE6,
        TRIM(soda.so_dl_text_7)      AS LINE7,
        TRIM(soda.so_dl_postcode)    AS POSTCODE,
        TRIM(soda.dl_country_code)   AS COUNTRY
      FROM sales_order_delivery_all AS soda
      WHERE soda.so_text_type = ?
        AND (soda.so_order_no = ? AND soda.so_bo_suffix = ?)
        AND soda.so_dl_text_1 != 'XXMULTDA'";
    $res = $odbc->query($sql, array($da_type, $order['num'], $order['suf']));
    if ($res) return array_values(array_filter($res[0]));
    return null;
  }
}
