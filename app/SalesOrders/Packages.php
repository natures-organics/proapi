<?php

namespace SalesOrders;

class Packages extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $res_packages = null;
    $soid   = $params['soid'];
    $pkgid  = ltrim($params['pkgid'], '0');

    if (!soid && !$pkgid)
      $f3->error(400);

    // split the order id into 'number' and 'suffix' parts because they are
    // different columns in the database.
    if ($soid) {
      if (!$this->is_valid_so_number($soid))
        $f3->error(422);
      $order['num'] = preg_replace('/[^\d*]/i', '', $soid);
      $order['suf'] = preg_replace('/[^A-Z*]/i', '', $soid);
    }

    $sql = "
      SELECT
        -- include the order number/suffix for when client is searching
        -- by sscc number (they probably dont know the order number)
        solp.so_order_no          AS ORDER_NUM,
        TRIM(solp.so_bo_suffix)   AS ORDER_SUF,
        solp.sol_line_seq         AS LINE_SEQ,
        TRIM(solp.sopk_sscc)      AS SSCC,
        solp.solpk_qty            AS PACKAGE_QTY,
        TRIM(sm.stock_code)       AS STKID,
        TRIM(sm.stk_description)  AS STKDESC,
        NVL(solpk.solpl_packed_qty, solp.solpk_qty) AS LOT_QTY,
        TRIM(solpk.lot_or_serial_no) AS LOT_NUMBER
      FROM sales_order_line_packages AS solp
      LEFT OUTER JOIN sales_order_line_pkg_lots AS solpk
        ON (solp.sopk_sscc = solpk.sopk_sscc)
      JOIN sales_order_line_all AS sol
        ON (sol.so_order_no = solp.so_order_no
        AND sol.so_bo_suffix = solp.so_bo_suffix
        AND sol.sol_line_seq = solp.sol_line_seq)
      LEFT JOIN stock_master AS sm ON (sol.stock_code = sm.stock_code)
      WHERE 1=1";
    $args = array();
    if ($soid) {
      $sql .= " AND (solp.so_order_no = ? AND solp.so_bo_suffix = ?)";
      $args[] = $order['num'];
      $args[] = $order['suf'];
    }
    if ($pkgid) {
      $sql .= " AND solp.sopk_sscc = ?";
      $args[] = $pkgid;
    }
    $sql .= " ORDER BY solp.so_order_no, solp.sol_line_seq, solp.sopk_sscc";
    $res_packages = $odbc->query($sql, $args);

    $data = $this->massage_arrays($res_packages);
    $this->return_data2client($data);
  }

  private function massage_arrays($packages) {
    $f3=\Base::instance();

    foreach ($packages as $pkg) {
      $sscc = $pkg['SSCC'];
      $pkg['LINE_SEQ'] = floatval($pkg['LINE_SEQ']);
      $pkg['PACKAGE_QTY'] = floatval($pkg['PACKAGE_QTY']);
      $pkg['PRODUCT'] = array(
        'ID'          => $pkg['STKID'],
        'DESCRIPTION' => $pkg['STKDESC'],
      );

      $lotnum = $pkg['LOT_NUMBER'];
      if ( $lotnum )
        $pkg['PRODUCT']['LOTS'][$lotnum] = floatval($pkg['LOT_QTY']);

      unset($pkg['SSCC'],
        $pkg['LOT_NUMBER'], $pkg['LOT_QTY'],
        $pkg['STKID'], $pkg['STKDESC']
      );
      $results[$sscc] = $pkg;
    }

    return $results;
  }
  
}
