<?php

namespace Suppliers;

class Index extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $where_type   = $f3->get('REQUEST.type') ?: null;
    $where_group  = $f3->get('REQUEST.group') ?: null;
    $where_status = $f3->get('REQUEST.status') ?: null;

    $sql = "
      SELECT
        TRIM(cm.cre_accountcode)      AS ID,
        TRIM(cm.cr_shortname)         AS NAME
      FROM cre_master AS cm
      WHERE 1=1\n";

    // optionally append WHERE clause(s) to sql query
    $args = array();
    if ( $where_type ) {
      $sql .= ' AND (UPPER(TRIM(cm.cr_type)) = UPPER(?))';
      $args[] = $where_type;
    }
    if ( $where_group ) {
      $sql .= ' AND (UPPER(TRIM(cm.cr_supplier_grp)) = UPPER(?))';
      $args[] = $where_group;
    }
    if ( $where_status ) {
      $sql .= ' AND (UPPER(TRIM(cm.account_status)) = UPPER(?))';
      $args[] = $where_status;
    }

    $sql .= "\nORDER BY cm.cre_accountcode";
    $res = $odbc->query($sql, $args);
    $this->return_data2client($res);
  }

}
