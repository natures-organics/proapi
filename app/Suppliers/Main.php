<?php

namespace Suppliers;

class Main extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['suppid'];

    $sql = "
      SELECT
        TRIM(cm.cre_accountcode)      AS SUPPLIER_CODE,
        TRIM(cm.cr_shortname)         AS SUPPLIER_NAME,
        TRIM(cm.cr_pay_to_code)       AS PARENT_CODE,
        TRIM(cm_parent.cr_shortname)  AS PARENT_NAME,
        TRIM(cm.account_status)       AS STATUS,
        CASE
          WHEN cm.account_status = '' THEN 'OK'
          WHEN cm.account_status = 'N' THEN 'On Hold'
          WHEN cm.account_status = 'H' THEN 'QA Hold'
          WHEN cm.account_status = 'I' THEN 'Stock only'
          WHEN cm.account_status = 'S' THEN 'Special only'
          WHEN cm.account_status = 'A' THEN 'Auto invoice'
          ELSE TRIM(cm.account_status)
        END AS STATUS_DESC,
        cm.last_purchase              AS LAST_PURCHASE_DATE,
        TRIM(cm.cr_contact_name)      AS CONTACT_NAME,
        -- address data
        TRIM(nam_address.na_company)  AS ADDR1,
        TRIM(nam_address.na_street)   AS ADDR2,
        TRIM(nam_address.na_suburb)   AS SUBURB,
        TRIM(nam_address.na_country)  AS COUNTRY,
        TRIM(nam_address.postcode)    AS POSTCODE,
        TRIM(nam_address.na_phone)    AS PHONE,
        TRIM(nam_address.na_phone_2)  AS MOBILE,
        TRIM(nam_email.na_name||nam_email.na_company) AS EMAIL,
        cm.last_changed               AS LAST_CHANGED_DATE,
        TRIM(cm.cr_curr_code)         AS CURRENCY,
        cm.terms_days,
        TRIM(cm.cr_type)              AS SUPP_TYPE,
        TRIM(cm.cr_supplier_grp)      AS SUPP_GROUP
      FROM cre_master AS cm
      LEFT JOIN cre_master AS cm_parent ON (cm.cr_pay_to_code = cm_parent.cre_accountcode)
      LEFT JOIN name_and_address_master AS nam_address ON (cm.cre_accountcode = nam_address.accountcode and nam_address.na_type = 'C')
      LEFT JOIN name_and_address_master AS nam_email ON (cm.cre_accountcode = nam_email.accountcode and nam_email.na_type = 'E')
      WHERE UPPER(cm.cre_accountcode) = UPPER(?)\n";

    // execute query, reformat results and send to the client
    $res = $odbc->query($sql, array($id));
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  /****************************************************************************
   * restructures the rows returned from the sql query to be multi-dimensional
   * so the returned json/xml has proper structure
   */
  private function massage_arrays($results) {
    foreach ($results as $row) {
      $id = $row['SUPPLIER_CODE'];
      if ($row['SUPPLIER_CODE'] != $row['PARENT_CODE']) {
        $row['PARENT'] = array(
          'ID' => $row['PARENT_CODE'],
          'SHORTNAME' => $row['PARENT_NAME'],
        );
      }
      unset($row['SUPPLIER_CODE'], $row['PARENT_CODE'], $row['PARENT_NAME']);

      $row['STATUS'] = array(
        'ID' => $row['STATUS'],
        'DESCRIPTION' => $row['STATUS_DESC'],
      );
      unset($row['STATUS_DESC']);

      // supplier address
      $row['ADDRESS'] = array(
        'LINE1'   => $row['ADDR1'],
        'LINE2'   => $row['ADDR2'],
        'SUBURB'  => $row['COUNTRY'],
        'POSTCODE'=> $row['POSTCODE'],
      );
      unset($row['ADDR1'], $row['ADDR2'], $row['SUBURB'], $row['POSTCODE'], $row['COUNTRY']);

      // contact details
      $row['CONTACT'] = array(
        'NAME'  => $row['CONTACT_NAME'],
        'PHONE' => $row['PHONE'],
        'MOBILE'=> $row['MOBILE'],
        'EMAIL' => $row['EMAIL'],
      );
      unset($row['CONTACT_NAME'], $row['PHONE'], $row['MOBILE'], $row['EMAIL']);

      // dates
      $row['DATE'] = array(
        'LAST_CHANGED'  => $row['LAST_CHANGED_DATE'] == '1899-12-31' ? null : $this->format_date($row['LAST_CHANGED_DATE']),
        'LAST_PURCHASE' => $row['LAST_PURCHASE_DATE']== '1899-12-31' ? null : $this->format_date($row['LAST_PURCHASE_DATE']),
      );
      unset($row['LAST_CHANGED_DATE'], $row['LAST_PURCHASE_DATE']);

      // meta details
      $row['TYPE']  = $row['SUPP_TYPE'];
      $row['GROUP'] = $row['SUPP_GROUP'];
      $row['TERMS_DAYS'] = floatval($row['TERMS_DAYS']);
      unset($row['SUPP_TYPE'], $row['SUPP_GROUP']);

      $data[$id] = $row;
    }

    return $data;
  }
}
