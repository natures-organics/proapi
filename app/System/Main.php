<?php

namespace System;

class Main extends \Controller {

  function get($f3,$params) {
    $id = strtoupper($params['id']);
    $key = strtoupper($params['key']);

    switch ($id) {
    case 'SQ':
      $data = $this->get_sq();
      break;
    case 'UM':
      $data = $this->get_um();
      break;
    case 'WH':
      $data = $this->get_wh();
      break;
    case 'SU':
      $data = $this->get_su();
      break;
    case 'TC':
    case 'CT':
    case 'CS':
    case 'RV':
    case 'B6':
    case 'CA':
    case 'CN':
    case 'CU':
    case 'SX':
    case 'SZ':
    case 'UN':
      $data = $this->get_sys_generic($id, $key);
      break;
    default:
      $f3->error(501);
    }

    $data = $this->massage_arrays($data);
    $this->return_data2client($data);
  }

  private function massage_arrays($results) {
    $new_data = array();
    foreach ($results as $row) {
      $id = $row['ID'];
      unset($row['ID']);
      $new_data[$id] = $row;
    }
    return $new_data;
  }

  private function get_sys_generic($table_id, $key) {
    $odbc = \ODBC::instance();
    $args = array($table_id);
    $sql = "SELECT
        TRIM(sys.sys_tbl_code) AS ID,
        TRIM(sys.sys_description) AS DESCRIPTION
      FROM system_table AS sys
      WHERE UPPER(TRIM(sys.sys_tbl_type)) = UPPER(?)\n";
    if ($key) {
      $sql .= "AND UPPER(TRIM(sys_tbl_code)) = UPPER(?)";
      $args[] = $key;
    }
    $sql .= "\nORDER BY sys.sys_tbl_code";
    return $odbc->query($sql, $args);
  }

  private function get_sq() {
    $odbc = \ODBC::instance();
    $sql = "SELECT
        TRIM(sys.sys_tbl_code) AS ID,
        TRIM(sys.sys_description) AS DESCRIPTION,
        sys.sys_money_value::integer AS TYPEID,
        CASE
          -- these literals are quoted because informix will pad the column to
          -- the longest value within the case
          WHEN sys.sys_money_value = 0 THEN TRIM('Lost')
          WHEN sys.sys_money_value = 1 THEN TRIM('Not Picked')
          WHEN sys.sys_money_value = 2 THEN TRIM('Over Supplied')
        END AS TYPEDESC
      FROM system_table AS sys
      WHERE TRIM(sys.sys_tbl_type) = 'SQ'
      ORDER BY sys.sys_tbl_code";
    return $odbc->query($sql);
  }

  private function get_um() {
    $odbc = \ODBC::instance();
    $sql = "SELECT
        TRIM(sys.sys_tbl_code) AS ID,
        TRIM(sys.sys_description) AS DESCRIPTION,
        TRIM(sys.sys_tbl_alpha_1) AS DECIMAL_OVERRIDE,
        CASE WHEN sys.sys_tbl_alpha_1 = 'Y' THEN sys.sys_money_value::integer ELSE null END AS DECIMALS
      FROM system_table AS sys
      WHERE TRIM(sys.sys_tbl_type) = 'UM'
      ORDER BY sys.sys_tbl_code";
    return $odbc->query($sql);
  }

  private function get_wh() {
    $odbc = \ODBC::instance();
    $sql = "SELECT
        TRIM(sys.sys_tbl_code) AS ID,
        TRIM(sys.sys_description) AS DESCRIPTION,
        TRIM(sys.sys_tbl_alpha_3) AS SOURCE,
        sys.tbl_sales_mtd::integer AS TYPE
      FROM system_table AS sys
      WHERE TRIM(sys.sys_tbl_type) = 'WH'
      ORDER BY sys.sys_tbl_code";
    return $odbc->query($sql);
  }

  private function get_su() {
    $odbc = \ODBC::instance();
    $sql = "SELECT
        TRIM(sys.sys_tbl_code) AS ID,
        TRIM(sys.sys_description) AS DESCRIPTION,
        sys.sys_money_value::integer AS DAYS,
        TRIM(sys.sys_tbl_alpha_2) AS BESTBEFORE_OR_USEBY
      FROM system_table AS sys
      WHERE TRIM(sys.sys_tbl_type) = 'SU'
      ORDER BY sys.sys_tbl_code";
    return $odbc->query($sql);
  }

}
