<?php

namespace WorkOrders;

class Dockets extends \Controller {

  function get($f3, $params) {
    $odbc = \ODBC::instance();
    $id = $params['woid'];

    $sql = "
      SELECT
        --ped.prod_entry_date,
        ped.prod_entry_type,
        ped.prod_whse_code,
        ped.comp_whse_code,
        ped.prod_entry_shift,
        ped.entry_status,
        ped.prod_entry_qty,
        ped.finish_flag,
        TRIM(ped.prod_entry_ref2) AS PROD_ENTRY_REF2,
        ped.entry_date_time,
        TRIM(ped.screen_type) AS SCREEN_TYPE,
        TRIM(ped.entry_user_id) AS USER_ID
      FROM production_entry_dockets AS ped
      WHERE TRIM(ped.docket_number) = ?
      ORDER BY ped.entry_date_time";
    $res = $odbc->query($sql, array($id));

    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }
  
  private function massage_arrays($dockets) {
    foreach ($dockets as $docket) {
      $docket['PROD_ENTRY_QTY'] = floatval($docket['PROD_ENTRY_QTY']);
      $docket['ENTRY_TIMESTAMP'] = $this->format_timestamp($docket['ENTRY_DATE_TIME']);
      $docket['WAREHOUSE'] = array(
        'SOURCE'  => $docket['COMP_WHSE_CODE'],
        'OUTPUT'  => $docket['PROD_WHSE_CODE'],
      );
      unset( $docket['ENTRY_DATE_TIME'],
        $docket['PROD_WHSE_CODE'], $docket['COMP_WHSE_CODE']
      );
      $results[] = $docket;
    }

    return $results;
  }

}
