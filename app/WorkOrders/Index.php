<?php

namespace WorkOrders;

class Index extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $where_product  = $f3->get('REQUEST.product') ?: null;
    $where_status   = $f3->get('REQUEST.status') ?: null;
    $where_warehouse= $f3->get('REQUEST.warehouse') ?: null;

    $sql = "
      SELECT  TRIM(wo.work_order)       AS WORK_ORDER,
              TRIM(wo.wo_parent)        AS PRODUCT_ID,
              wo.wo_status              AS STATUS_ID
      FROM bom_work_order AS wo
      WHERE 1=1";
    $args = array();
    if ( $where_product ) {
      $sql .= ' AND (UPPER(TRIM(wo.wo_parent)) = UPPER(?))';
      $args[] = $where_product;
    }
    if ( $where_status ) {
      $sql .= ' AND (wo.wo_status = ?)';
      $args[] = $where_status;
    }
    if ( $where_warehouse ) {
      $sql .= ' AND (UPPER(TRIM(wo.warehouse_code)) = UPPER(?))';
      $args[] = $where_warehouse;
    }
    if (!$args) $f3->error(400); // no filtering given; refuse to return entire table
    
    $sql .= "\nORDER BY wo.work_order";
    $this->return_data2client($odbc->query($sql, $args));
  }

}
