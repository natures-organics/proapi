<?php

namespace WorkOrders;

class Lines extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['woid'];

    $sql = "
      SELECT  TRIM(wot.trn_code) AS TRN_CODE,
              -- db stores seq numbers as '240001' instead of '24' (wtaf?)
              CAST(wot.wo_seq_no/10000 AS integer) AS SEQ,
              TRIM(wot.comp_code) AS COMP_CODE,
              TRIM(sm_comp.stk_description) AS COMP_DESC,
              wot.date_required,
              wot.trn_date,
              wot.trn_whse,
              wot.trn_qty,
              wot.update_flag,
              TRIM(wot.trn_batch_ref) AS TRAN_BATCH_REF
      FROM bom_work_order_trans AS wot
      LEFT JOIN stock_master AS sm_comp ON (wot.comp_code = sm_comp.stock_code)
      WHERE (TRIM(wot.work_order) = ?)
        --AND TRIM(wot.trn_code) = 'COM' -- committed
      ORDER BY wot.wo_seq_no";
    $res = $odbc->query($sql, array($id));
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  private function massage_arrays($lines) {
    foreach ($lines as $line) {
      $line['DATE_REQUIRED'] = ($line['DATE_REQUIRED'] == '1899-12-31' ? null : $line['DATE_REQUIRED']);
      $line['SEQ'] = floatval($line['SEQ']);
      $line['TRN_QTY'] = floatval($line['TRN_QTY']);
      $results[] = $line;
    }

    return $results;
  }

}
