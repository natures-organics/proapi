<?php

namespace WorkOrders;

class Main extends \Controller {

  function get($f3,$params) {
    $odbc = \ODBC::instance();
    $id = $params['woid'];

    $sql = "
      SELECT  TRIM(wo.work_order)       AS WORK_ORDER,
              TRIM(wo.wo_parent)        AS STK_ID,
              TRIM(sm.stk_description)  AS STK_DESCRIPTION,
              wo.wo_status              AS STATUS_ID,
              bwosd.description         AS STATUS_DESC,
              wo.wo_qty_ordered,
              wo.wo_qty_completed,
              wo.wo_qty_rejected,
              wo.wo_qty_qc,
              wo.warehouse_code         AS OUTPUT_WAREHOUSE,
              wo.wo_factory_whse        AS FACTORY_WAREHOUSE,
              wo.wo_date,
              TRIM(wo.document_type) AS document_type,
              TRIM(wo.order_code) AS order_code,
              TRIM(wo.document_suffix) AS document_suffix,
              wo.updated,
              wo.expected_start,
              wo.expected_fin,
              wo.expected_dur_hrs,
              wo.start_date,
              wo.finish_date,
              TRIM(wo.wo_user_name) AS wo_user_name,
              wo.date_last_change,
              wo.wo_bom_id AS BOM_ID
      FROM bom_work_order AS wo
      LEFT JOIN stock_master AS sm ON (wo.wo_parent = sm.stock_code)
      LEFT JOIN bom_work_order_status_desc AS bwosd ON (wo.wo_status = bwosd.status_desc AND bwosd.bomwosd_language = '')
      WHERE (TRIM(wo.work_order) = ?)";
    $args = array($id);
    $res = $odbc->query($sql, $args);
    $data = $this->massage_arrays($res);
    $this->return_data2client($data);
  }

  private function massage_arrays($headers) {
    foreach ($headers as $wo) {
      $wo_id = $wo['WORK_ORDER'];
      $wo['UPDATED'] = floatval($wo['UPDATED']);

      $wo['PRODUCT'] = array(
        'ID'          => $wo['STK_ID'],
        'DESCRIPTION' => $wo['STK_DESCRIPTION'],
      );

      $wo['STATUS'] = array(
        'ID' => $wo['STATUS_ID'],
        'DESCRIPTION' => $wo['STATUS_DESC'],
      );
      unset($wo['STATUS_ID'], $wo['STATUS_DESC']);

      $wo['WAREHOUSE'] = array(
        'OUTPUT' => $wo['OUTPUT_WAREHOUSE'],
        'FACTORY' => $wo['FACTORY_WAREHOUSE'],
      );
      unset($wo['OUTPUT_WAREHOUSE'], $wo['FACTORY_WAREHOUSE']);

      $wo['QUANTITIES'] = array(
        'ORDERED'   => floatval($wo['WO_QTY_ORDERED']),
        'COMPLETED' => floatval($wo['WO_QTY_COMPLETED']),
        'REJECTED'  => floatval($wo['WO_QTY_REJECTED']),
        'QC'        => floatval($wo['WO_QTY_QC']),
        'UPDATED'   => floatval($wo['UPDATED']),
      );
      $wo['DATES'] = array(
        'CREATED'           => $this->format_date($wo['WO_DATE']),
        'ACTUAL_START'      => $wo['START_DATE'] == '1899-12-31' ? null : $this->format_date($wo['START_DATE']),
        'ACTUAL_FINISH'     => $wo['FINISH_DATE'] == '1899-12-31' ? null : $this->format_date($wo['FINISH_DATE']),
        'EXPECTED_START'    => $wo['EXPECTED_START'] == '1899-12-31' ? null : $this->format_date($wo['EXPECTED_START']),
        'EXPECTED_FINISH'   => $wo['EXPECTED_FIN'] == '1899-12-31' ? null : $this->format_date($wo['EXPECTED_FIN']),
        'EXPECTED_DURATION' => floatval($wo['EXPECTED_DUR_HRS']),
        'LAST_CHANGE'       => $wo['DATE_LAST_CHANGE'] == '1899-12-31' ? null : $this->format_date($wo['DATE_LAST_CHANGE']),
      );
      unset(
        $wo['STK_ID'], $wo['STK_DESCRIPTION'],
        $wo['WORK_ORDER'], $wo['WO_DATE'],
        $wo['WO_QTY_ORDERED'], $wo['WO_QTY_COMPLETED'],
        $wo['WO_QTY_REJECTED'], $wo['WO_QTY_QC'], $wo['UPDATED'],
        $wo['EXPECTED_START'], $wo['EXPECTED_FIN'], $wo['EXPECTED_DUR_HRS'],
        $wo['START_DATE'], $wo['FINISH_DATE'], $wo['DATE_LAST_CHANGE']
      );
      $results[$wo_id] = $wo;
    }

    return $results;
  }

}
