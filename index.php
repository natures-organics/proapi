<?php

// set our working directory to the base of index.php
chdir(__DIR__);

// Kickstart composer autoloader
require_once 'vendor/autoload.php';
$f3 = \Base::instance();

$f3->set('PACKAGE', 'proapi');
$f3->set('AUTOLOAD','app/');
$f3->set('LOGS',    'logs/');
$f3->set('TEMP',    'tmp/');
$f3->set('UI',      'views/');

if ((float)PCRE_VERSION<7.9)
  trigger_error('PCRE version is out of date');

/*
 * load configuration
 */
$f3->config('proapi.cfg');
$f3->config('proapi.cfg.local');
$f3->config('maps.cfg');
$f3->set('TZ', $f3->get('timezone'));

/*
 * set default values for any undefined config directives
 */
$f3->set('max_results',     $f3->get('max_results') ?: 1);
$f3->set('default_format',  $f3->get('default_format') ?: 'json');

/*
 * set up our custom error handler
 */
$f3->set('ONERROR', function($f3) {
    // recursively clear existing output buffers
    if ($f3->get('DEBUG') == 0 )
      while (ob_get_level())
        ob_end_clean();

    echo \Template::instance()->render('error.htm');
  }
);

/*
 * authenticate the client
 */
$auth = new Authentication;
$auth->authenticate_client();

/*
 * instantiate the ODBC connection
 */
$odbc = \ODBC::instance();
$odbc->set_host($f3->get('odbc.host'));
$odbc->set_port($f3->get('odbc.port'));
$odbc->set_dbname($f3->get('REQUEST.db') ?:   // allow the user to override
  $auth->key_info($f3->get('apikey'))['db']); // default to the apikey's default
$odbc->set_locale($f3->get('odbc.locale'));
$odbc->set_instance($f3->get('odbc.instance'));
$odbc->set_cache_timeout($f3->get('config.sql_cache_timeout') ?: 0);
if (!$odbc->connect($f3->get('odbc.username'), $f3->get('odbc.password')))
  $f3->error(500, 'Unable to create ODBC connection');

$f3->run();
